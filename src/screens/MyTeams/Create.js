// IMPORT libs
import React from "react";
// IMPORT components
import CreateTeamStepper from "components/Team/Create/Stepper";
import AppHeaderCloseSave from "components/AppHeader/CloseOrBackSaveOrEdit";
// Screen Stateless
const ScreenCreateTeam = props => (
  <div>
    <AppHeaderCloseSave closeIcon formattedTitle="create-team" />
    <CreateTeamStepper />
  </div>
);

export default ScreenCreateTeam;
