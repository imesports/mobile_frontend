import styled from "styled-components";

const Centralized = styled.div`
   {
    position: absolute;
    height: 100%;
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export default Centralized;
