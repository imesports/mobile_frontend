// IMPORT libs
import React from "react";
import { connect } from "react-redux";
// IMPORT icons
import AccountMultiplePlusIcon from "mdi-react/AccountMultiplePlusIcon";
// import MagnifyIcon from "mdi-react/MagnifyIcon";
// IMPORT components
import TeamListAvatarGameMembers from "components/Team/List/AvatarGameMembers";
import PlusFABWithList from "components/UI/Button/PlusFABWithList";
import WithFABWrapper from "components/UI/Wrapper/WithFABWrapper";
import { LoggedInWrapper } from "components/UI/Wrapper";

const list = [
  {
    title: "create-team",
    goTo: "/my-teams/create",
    icon: <AccountMultiplePlusIcon />
  }
  // {
  //   title: "search-team",
  //   goTo: "/search",
  //   icon: <MagnifyIcon />
  // }
];

const ScreenMyTeams = props => (
  <LoggedInWrapper>
    <WithFABWrapper>
      <TeamListAvatarGameMembers {...props} />
      <PlusFABWithList list={list} variant="fab" />
    </WithFABWrapper>
  </LoggedInWrapper>
);

const mapStateToProps = state => ({
  teamListLoading: state.app.teamListLoading,
  teamsList: state.user.teams,
  userId: state.user.profile.user
});

export default connect(mapStateToProps)(ScreenMyTeams);
