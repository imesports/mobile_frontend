import Typography from "material-ui/Typography";
import styled from "styled-components";

export const RoleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${props =>
    props.flexStart ? "flex-start" : "space-between"};
`;

export const DescriptionWrapper = styled.div`
  margin: 0 ${props => props.theme.spacing.default}px;
`;

export const RequisitesItem = styled(Typography)`
  && {
    margin-bottom: ${props => props.theme.spacing.xs}px;
  }
`;

export const RequisitesTitle = styled(Typography)`
  && {
    font-weight: 600;
  }
`;
