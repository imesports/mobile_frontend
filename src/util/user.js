import CONSTANTS from "appConstants";

export const signUpFetch = body => {
  return fetch(`${CONSTANTS.BACKEND_URL}/users/`, {
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    body: JSON.stringify(body)
  });
};

//TODO: Update URL
export const logInFetch = body =>
  fetch(`${CONSTANTS.BACKEND_URL}/token/login/`, {
    method: "POST",
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify(body)
  });

export const profileFetch = (token, userId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-profile/${userId}/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });

export const updateProfile = (token, userId, body) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-profile/${userId}/`, {
    method: "PATCH",
    headers: {
      Authorization: `JWT ${token}`
    },
    body
  });
