// IMPORT libs
import AppBar from "material-ui/AppBar";
import styled from "styled-components";

// Custom Styles
export const ThemedAppBar = styled(AppBar)`
  && {
    background: ${props => props.theme.background.appbar};
  }
`;
