// IMPORT libs
import React, { Component } from "react";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import TextField from "material-ui/TextField";
import Toolbar from "material-ui/Toolbar";
// IMPORT components
import ToolbarAvatar from "components/UI/Avatar/Toolbar";
// Custom Styles
const SearchTextField = styled(TextField)`
  flex: 1;
`;

const StyledToolbar = styled(Toolbar)`
  && {
    min-height: ${props => props.theme.headerSize.commonToolbar}px;
    height: ${props => props.theme.headerSize.commonToolbar}px;
  }
`;

// Component: Stateful
class SearchToolbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: ""
    };
  }
  render() {
    const {
      placeholder,
      intl,
      toggleDrawer,
      thumbnail,
      isLoading
    } = this.props;
    const { searchText } = this.state;
    return (
      <StyledToolbar>
        <ToolbarAvatar
          isLoading={isLoading}
          onClick={() => toggleDrawer("showMobileDrawer", true)}
          thumbnail={thumbnail}
        />
        <SearchTextField
          id="search"
          type="search"
          placeholder={intl.formatMessage({ id: placeholder })}
          value={searchText}
          onChange={this.handleChange("searchText")}
        />
      </StyledToolbar>
    );
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
}

// PropTypes
SearchToolbar.propTypes = {
  placeholder: PropTypes.string.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  thumbnail: PropTypes.string
};

export default injectIntl(SearchToolbar);
