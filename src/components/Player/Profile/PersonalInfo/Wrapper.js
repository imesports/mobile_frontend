import styled from "styled-components";

export const AvatarPosition = styled.div`
   {
    position: absolute;
    bottom: calc(-${props => props.theme.iconSize.xxxl}px / 2);
    left: calc(50% - ${props => props.theme.iconSize.xxxl}px / 2);
    z-index: 3;
  }
`;

export const AvatarChangeBackground = styled.div`
   {
    border-radius: ${props => props.theme.iconSize.xxxl}px;
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    z-index: 4;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const FeatureImageChangeBackground = styled.div`
   {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.4);
    z-index: 2;
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
    padding: ${props => props.theme.spacing.small}px;
  }
`;

export const EditProfilePosition = styled.div`
   {
    position: absolute;
    bottom: calc(
      0px - ${props => props.theme.iconSize.medium}px -
        ${props => props.theme.spacing.small}px
    );
    right: ${props => props.theme.spacing.small}px;
  }
`;
