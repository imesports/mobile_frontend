// IMPORT libs
import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import CameraIcon from "mdi-react/CameraIcon";
import PencilIcon from "mdi-react/PencilIcon";
import { CardMedia } from "material-ui/Card";
// IMPORT constants
import CONSTANTS from "appConstants";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { DefaultIcon } from "components/UI/IconFormatter";
import CardContent from "components/UI/Card/Content";
// IMPORT from same folder
import { TitleTypography, NameTypography } from "./Typography";
import {
  FeatureImageChangeBackground,
  AvatarChangeBackground,
  EditProfilePosition,
  AvatarPosition
} from "./Wrapper";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
   {
    width: 100%;
    padding-bottom: 50%;
    position: relative;
    margin-bottom: calc(${props => props.theme.iconSize.xxxl}px / 2);
  }
  @media screen and (min-width: 600px) {
    padding-bottom: ${props => props.theme.cardMediaSize.xxl}px;
  }
`;

const StyledCardContent = styled(CardContent)`
  &&& {
    width: 100%;
    text-align: center;
    overflow: hidden;
  }
`;
// Component: Stateless
const ProfileHeader = ({
  avatar,
  background_image,
  first_name,
  display_name,
  last_name,
  headline,
  location,
  changeBackground,
  changeAvatar,
  editProfileOption
}) => (
  <div>
    <StyledCardMedia
      image={background_image || CONSTANTS.DEFAULT_BACKGROUND_IMAGE}
    >
      {changeBackground && (
        <FeatureImageChangeBackground
          onClick={() => document.getElementById("upload-bg-picture").click()}
        >
          <DefaultIcon size="medium">
            <CameraIcon />
          </DefaultIcon>
        </FeatureImageChangeBackground>
      )}
      <AvatarPosition
        onClick={() => document.getElementById("upload-avatar-picture").click()}
      >
        {changeAvatar && (
          <AvatarChangeBackground>
            <DefaultIcon size="medium">
              <CameraIcon />
            </DefaultIcon>
          </AvatarChangeBackground>
        )}
        <CustomSizeCircleAvatar
          src={avatar || CONSTANTS.DEFAULT_AVATAR_IMAGE}
          size="xxxl"
          id="signup-username-avatar"
        />
      </AvatarPosition>
      {editProfileOption && (
        <EditProfilePosition>
          <DefaultIcon size="sm">
            <Link to="./edit-profile">
              <PencilIcon />
            </Link>
          </DefaultIcon>
        </EditProfilePosition>
      )}
    </StyledCardMedia>
    <StyledCardContent>
      <NameTypography variant="headline">
        {first_name} {display_name && <span>&ldquo;{display_name}&rdquo;</span>}{" "}
        {last_name}
      </NameTypography>
      <TitleTypography variant="subheading" noWrap>
        {headline}
      </TitleTypography>
      <Typography color="primary" variant="subheading" noWrap>
        {location}
      </Typography>
    </StyledCardContent>
  </div>
);

// PropTypes
ProfileHeader.propTypes = {
  avatar: PropTypes.string.isRequired,
  background_image: PropTypes.string.isRequired,
  first_name: PropTypes.string.isRequired,
  display_name: PropTypes.string.isRequired,
  last_name: PropTypes.string.isRequired,
  headline: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  changeBackground: PropTypes.bool,
  changeAvatar: PropTypes.bool,
  editProfileOption: PropTypes.bool
};

export default ProfileHeader;
