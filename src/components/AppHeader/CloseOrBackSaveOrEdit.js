// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
// IMPORT components
import CloseOrBackSaveOrEditToolbar from "components/UI/Toolbar/CloseOrBackSaveOrEdit";
import { ThemedAppBar } from "components/UI/AppBar";
// Component: Stateless
const AppHeaderCloseOrBackSaveOrEdit = ({
  title,
  formattedTitle,
  isLoading,
  closeIcon,
  backIcon,
  closeIconAction,
  saveButton,
  saveAction,
  editIcon
}) => (
  <ThemedAppBar>
    <CloseOrBackSaveOrEditToolbar
      title={title}
      formattedTitle={formattedTitle}
      closeIconAction={closeIconAction}
      closeIcon={closeIcon}
      backIcon={backIcon}
      saveButton={saveButton}
      saveAction={saveAction}
      editIcon={editIcon}
      isLoading={isLoading}
    />
    {isLoading && <LinearProgress color="secondary" />}
  </ThemedAppBar>
);

// PropTypes
AppHeaderCloseOrBackSaveOrEdit.propTypes = {
  title: PropTypes.string,
  formattedTitle: PropTypes.string,
  closeIcon: PropTypes.bool,
  backIcon: PropTypes.bool,
  saveButton: PropTypes.bool,
  saveAction: PropTypes.func,
  closeIconAction: PropTypes.func,
  editIcon: PropTypes.bool
};

export default AppHeaderCloseOrBackSaveOrEdit;
