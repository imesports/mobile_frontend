// IMPORT libs
import React from "react";
// IMPORT components
import { DividerDefaultMargin } from "components/UI/Divider";
import MemberItem from "components/Team/Item/Member";
// Stateless component
const TeamMembers = props => {
  const { teamPositions } = props;
  const teamMembers = teamPositions.length;

  return (
    <div>
      {teamMembers > 0 &&
        teamPositions.map((content, index) => (
          <div key={`TeamMemberItem ${content.player_profile.user}`}>
            <MemberItem
              {...content.player_profile}
              gameRole={content.game_role}
            />
            {teamMembers !== index + 1 && <DividerDefaultMargin />}
          </div>
        ))}
    </div>
  );
};

export default TeamMembers;
