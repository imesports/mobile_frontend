// IMPORT libs
import React, { Component } from "react";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Card, { CardHeader } from "material-ui/Card";
import { withStyles } from "material-ui/styles";
import classnames from "classnames";
// IMPORT icons
import ChevronDownIcon from "mdi-react/ChevronDownIcon";
import Collapse from "material-ui/transitions/Collapse";
import IconButton from "material-ui/IconButton";
import Divider from "material-ui/Divider";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import CardContent from "components/UI/Card/Content";
import { ThickDivider } from "components/UI/Divider";
// IMPORT from same folder
import OpenRoleCard from "./OpenRoles/Card";
// Custom Styles
const styles = theme => ({
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: "auto"
  },
  expandOpen: {
    transform: "rotate(180deg)"
  }
});

const StyledDivider = styled(Divider)`
  && {
    margin: ${props => props.theme.spacing.default}px 0;
  }
`;

const StyledHeader = styled(CardHeader)`
  background-color: ${props => props.theme.background.innerCard};
`;
// Component: Stateful
class TeamVacancyRoleCard extends Component {
  constructor(props) {
    super(props);
    this.state = { expanded: false };
  }
  render() {
    const { classes, data, intl } = this.props;
    const rowLength = data.positions.length;
    return (
      <Card>
        <StyledHeader
          onClick={this.handleExpandClick}
          avatar={<CustomSizeCircleAvatar size="xl" src={data.teamImg} />}
          action={
            <IconButton
              className={classnames(classes.expand, {
                [classes.expandOpen]: this.state.expanded
              })}
              aria-expanded={this.state.expanded}
              aria-label={intl.formatMessage({ id: "show-more" })}
            >
              <ChevronDownIcon />
            </IconButton>
          }
          title={data.teamName}
          subheader={`${data.address.city}, ${data.address.state} -
          ${data.address.country}`}
        />
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <ThickDivider />
          <CardContent>
            {data.positions.map((item, index) => (
              <div key={index}>
                <OpenRoleCard
                  role={item.role}
                  avatar={item.avatar}
                  data={item.requirements}
                  teamInfo={{ teamName: data.teamName, teamImg: data.teamImg }}
                />
                {rowLength !== index + 1 && <StyledDivider />}
              </div>
            ))}
          </CardContent>
        </Collapse>
      </Card>
    );
  }

  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };
}

// PropTypes
TeamVacancyRoleCard.propTypes = {
  data: PropTypes.object.isRequired
};

export default injectIntl(withStyles(styles)(TeamVacancyRoleCard));
