import IconButton from "material-ui/IconButton";
import Button from "material-ui/Button";
import styled from "styled-components";

export const ThemedButton = styled(Button)`
  && {
    float: right;
  }
`;

export const ThemedIconButton = styled(IconButton)`
  && svg {
    fill: ${props => props.theme.icon.default};
  }
`;

export const RaisedAccentButton = styled(Button)`
  &&& {
    background: ${props => props.theme.primary.default};
    color: ${props => props.theme.text.button};
  }
`;

export const AccentButton = styled(Button)`
  &&& {
    color: ${props => props.theme.primary.default};
  }
  &&& :after {
    background: ${props => props.theme.primary.default};
  }
`;

export const SmallAccentButton = styled(Button)`
  &&& {
    color: ${props => props.theme.primary.default};
    text-transform: none;
    padding: 0 ${props => props.theme.spacing.xs}px;
    min-height: 0px;
  }
  &&& :after {
    background: ${props => props.theme.primary.default};
    padding: ${props => props.theme.spacing.xs}px;
  }
`;
