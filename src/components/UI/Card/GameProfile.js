// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Card, { CardMedia } from "material-ui/Card";
import Typography from "material-ui/Typography";
// IMPORT icons
import VerifiedIcon from "mdi-react/VerifiedIcon";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { DefaultIcon, NoButtonIcon } from "components/UI/IconFormatter";
import CardWrapper from "components/UI/Wrapper/Card";
import CardContent from "components/UI/Card/Content";
import {
  FirstRow,
  SecondRow,
  AvatarPosition,
  StayTogetherWrapper
} from "components/UI/Wrapper/GameProfile";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
  height: ${props => props.theme.cardMediaSize.medium}px;
  position: relative;
`;

const StyledCardContent = styled(CardContent)`
  &&& {
    padding-top: calc(
      (${props => props.theme.iconSize.xl}px / 2) +
        ${props => props.theme.spacing.default}px
    );
  }
`;
// Component: Stateless
const GameProfileCard = ({
  smallIconText,
  avatarImage,
  cardImage,
  verified,
  stats,
  name,
  intl
}) => (
  <CardWrapper>
    <Card>
      <StyledCardMedia image={cardImage}>
        <AvatarPosition>
          <CustomSizeCircleAvatar
            size="xl"
            src={avatarImage}
            title={intl.formatMessage({ id: name })}
          />
        </AvatarPosition>
      </StyledCardMedia>
      <StyledCardContent>
        <FirstRow>
          <Typography variant="title">{name}</Typography>
          {verified && (
            <NoButtonIcon size="medium" fill="verified">
              <VerifiedIcon />
            </NoButtonIcon>
          )}
        </FirstRow>
        <SecondRow>
          <StayTogetherWrapper>
            <NoButtonIcon size="small" fill="iconColor">
              <this.props.smallIcon />
            </NoButtonIcon>
            <Typography variant="caption">
              <FormattedMessage id={smallIconText} />
            </Typography>
          </StayTogetherWrapper>
          <Typography variant="caption">{stats}</Typography>
          {this.props.mediumIcon && (
            <DefaultIcon size="medium" fill="iconColor">
              <this.props.mediumIcon />
            </DefaultIcon>
          )}
        </SecondRow>
      </StyledCardContent>
    </Card>
  </CardWrapper>
);

// PropTypes
GameProfileCard.propTypes = {
  smallIconText: PropTypes.string.isRequired,
  avatarImage: PropTypes.string.isRequired,
  cardImage: PropTypes.string.isRequired,
  smallIcon: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  verified: PropTypes.string,
  mediumIcon: PropTypes.func,
  stats: PropTypes.string
};

export default injectIntl(GameProfileCard);
