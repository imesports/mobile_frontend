/* IMPORT libs */
import React from "react";
import { connect } from "react-redux";
/* IMPORT components */
import ProfileBasicInformation from "components/Team/Card/ProfileBasicInformation";
import TeamMembersList from "components/Team/List/TeamMembers";
import CollapsableCard from "components/UI/Card/Collapsable";
/* IMPORT styles*/
import { LoggedInWrapper } from "components/UI/Wrapper";
/* Stateles component */
const TeamProfileInformation = props => {
  const { teamInfo } = props;

  return (
    <LoggedInWrapper>
      <ProfileBasicInformation {...teamInfo} />
      <CollapsableCard formattedTitle="members">
        <TeamMembersList teamPositions={teamInfo.positions} />
      </CollapsableCard>
    </LoggedInWrapper>
  );
};
/* Redux */
const mapStateToProps = state => ({
  teamInfo: state.temporary.teamInfo
});
/* Export */
export default connect(mapStateToProps)(TeamProfileInformation);
