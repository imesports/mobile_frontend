import ShowTheWorldImg from "../../imgs/landing-page/show-the-world.jpg";
import ParticipateInATeamImg from "../../imgs/landing-page/participate-in-a-team.jpg";
import EnterChampionshipsImg from "../../imgs/landing-page/enter-championships.jpg";

import PubgIcon from "../../imgs/games/PubgIcon.jpg";
import LOLIcon from "../../imgs/games/LOLIcon.jpg";
import CSGOIcon from "../../imgs/games/CSGOIcon.jpg";
import DOTA2Icon from "../../imgs/games/DOTA2Icon.jpg";

import GuilhermePhoto from "../../imgs/team/guilherme.jpg";
import GiovanniPhoto from "../../imgs/team/giovanni.jpg";
import JorgePhoto from "../../imgs/team/jorge.jpg";
import RaulPhoto from "../../imgs/team/raul.jpg";

import FacebookBoxIcon from "mdi-react/FacebookBoxIcon";
import InstagramIcon from "mdi-react/InstagramIcon";
import TwitterBoxIcon from "mdi-react/TwitterBoxIcon";
import LinkedinBoxIcon from "mdi-react/LinkedinBoxIcon";

export default {
  socialNetworks: [
    {
      link: "https://www.facebook.com/IMeSportsGG/",
      icon: FacebookBoxIcon
    },
    {
      link: "https://www.instagram.com/IMeSportsGG/",
      icon: InstagramIcon
    },
    {
      link: "https://twitter.com/IMeSportsGG/",
      icon: TwitterBoxIcon
    },
    {
      link: "https://www.linkedin.com/company/11379697/",
      icon: LinkedinBoxIcon
    }
  ],

  gamesList: [
    {
      listItemImage: PubgIcon,
      listItemTitle: "pubg",
      listItemSubTitle: "landing-page.pubg-available-on"
    },
    {
      listItemImage: LOLIcon,
      listItemTitle: "lol",
      listItemSubTitle: "landing-page.lol-available-on"
    },
    {
      listItemImage: CSGOIcon,
      listItemTitle: "csgo",
      listItemSubTitle: "landing-page.csgo-available-on"
    },
    {
      listItemImage: DOTA2Icon,
      listItemTitle: "dota2",
      listItemSubTitle: "landing-page.dota2-available-on"
    }
  ],

  featuresList: [
    {
      cardImage: ShowTheWorldImg,
      title: "landing-page.show-the-world",
      subtitle: "landing-page.show-the-world.info"
    },
    {
      cardImage: ParticipateInATeamImg,
      title: "landing-page.participate-in-a-team",
      subtitle: "landing-page.participate-in-a-team.info"
    },
    {
      cardImage: EnterChampionshipsImg,
      title: "landing-page.enter-championships",
      subtitle: "landing-page.enter-championships.info"
    }
  ],

  imeSportsTeamList: [
    {
      listItemImage: GuilhermePhoto,
      listItemTitle: "guilherme",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/guigmen/"
    },
    {
      listItemImage: GiovanniPhoto,
      listItemTitle: "giovanni",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/giovannicimolin/"
    },
    {
      listItemImage: JorgePhoto,
      listItemTitle: "jorge",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/jorgelourenci/"
    },
    {
      listItemImage: RaulPhoto,
      listItemTitle: "raul",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/raulguedert/"
    }
  ]
};
