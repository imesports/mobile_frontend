// IMPORT libs
import React from "react";
// IMPORT icons
import AccountCircle from "mdi-react/AccountCircleIcon";
// IMPORT components
import { ThemedIconButton } from "components/UI/Button";
// IMPORT from same folder
import CustomSizeCircleAvatar from "./CustomSizeCircle";
// Component: Stateless
const ToolbarAvatar = ({ thumbnail = null, onClick, isLoading = false }) => (
  <ThemedIconButton onClick={onClick} disabled={isLoading}>
    {thumbnail === null ? (
      <AccountCircle />
    ) : (
      <CustomSizeCircleAvatar size="sm" src={thumbnail} />
    )}
  </ThemedIconButton>
);

export default ToolbarAvatar;
