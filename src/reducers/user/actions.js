// Imports from libs
import { push } from "react-router-redux";
import jwt_decode from "jwt-decode";

// Imports constants
import CONSTANTS from "appConstants";
import * as types from "./actionTypes";

// Import functions
import {
  loading,
  gameListLoading,
  teamListLoading
} from "reducers/app/actions";
import * as UserUtil from "util/user";
import { playerGamesFetch } from "util/game";
import { playerTeamsFetch } from "util/team";

export const updateToken = token => ({
  type: types.UPDATE_TOKEN,
  token
});

export const updateLoggedInProfile = profile => ({
  type: types.UPDATE_LOGGED_IN_PROFILE,
  profile
});

export const updatePlayerGames = games => ({
  type: types.UPDATE_PLAYER_GAMES,
  games
});

export const updatePlayerTeams = teams => ({
  type: types.UPDATE_PLAYER_TEAMS,
  teams
});

export const addPlayerTeam = team => ({
  type: types.ADD_PLAYER_TEAM,
  team
});

export const logout = () => ({
  type: types.LOGOUT
});

export const getPlayerGames = token => dispatch => {
  dispatch(gameListLoading(true));
  const decoded = jwt_decode(token);

  playerGamesFetch(token, decoded.user_id).then(res => {
    dispatch(gameListLoading(false));
    if (res.ok) {
      res.json().then(playerGames => {
        dispatch(updatePlayerGames(playerGames.games));
      });
    }
    //TODO: treatment of error
  });
};

export const getPlayerTeams = token => dispatch => {
  dispatch(teamListLoading(true));
  const decoded = jwt_decode(token);

  playerTeamsFetch(token, decoded.user_id).then(res => {
    dispatch(teamListLoading(false));
    if (res.ok) {
      res.json().then(playerTeams => {
        dispatch(updatePlayerTeams(playerTeams));
      });
    }
    //TODO: treatment of error
  });
};

export const logoutUser = () => dispatch => {
  localStorage.removeItem(CONSTANTS.AUTH_TOKEN);
  dispatch(logout());
  dispatch(push("/"));
};

export const savePlayerProfile = (token, formData, goTo) => dispatch => {
  const decoded = jwt_decode(token);
  dispatch(loading(true));
  UserUtil.updateProfile(token, decoded.user_id, formData).then(response => {
    dispatch(loading(false));
    if (response.ok) {
      response.json().then(response => {
        dispatch(updateLoggedInProfile(response));
        dispatch(push(goTo));
      });
    }
    //TODO: treatment of error
  });
};

export const signUp = body => dispatch => {
  dispatch(loading(true));

  UserUtil.signUpFetch(body).then(res => {
    dispatch(loading(false));
    if (res.ok) {
      res.json().then(response => {
        dispatch(saveInfoToLocalStorageAndRedux(response));
        dispatch(push("/signup/last-step"));
      });
    }
    //TODO: treatment of error
  });
};

export const logIn = body => dispatch => {
  dispatch(loading(true));

  UserUtil.logInFetch(body).then(res => {
    dispatch(loading(false));
    if (res.ok) {
      res.json().then(response => {
        dispatch(getPlayerGames(response.token));
        dispatch(getPlayerTeams(response.token));
        dispatch(saveInfoToLocalStorageAndRedux(response));
        dispatch(getProfile({ token: response.token, goTo: "/home" }));
      });
    }
    //TODO: treatment of error
  });
};

const saveInfoToLocalStorageAndRedux = response => dispatch => {
  localStorage.setItem(CONSTANTS.AUTH_TOKEN, response.token);
  const decoded = jwt_decode(response.token);
  const userInfo = {
    email: decoded.email
  };
  dispatch(updateLoggedInProfile(userInfo));
  dispatch(updateToken(response.token));
};

// TODO: Error handling
export const getProfile = ({
  token,
  userId = null,
  goTo = null
}) => dispatch => {
  const decoded = jwt_decode(token);
  const searchId = userId ? userId : decoded.user_id;
  dispatch(loading(true));

  UserUtil.profileFetch(token, searchId).then(res => {
    if (res.ok) {
      res.json().then(profile => {
        dispatch(loading(false));
        if (userId === null) {
          const userInfo = { ...profile, email: decoded.email };
          dispatch(updateLoggedInProfile(userInfo));
        }
        if (goTo) dispatch(push(goTo));
      });
    }
    //TODO: treatment of error
  });
};
