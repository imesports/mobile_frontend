// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Button from "material-ui/Button";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";
// Component: Stateless
const AlertDialog = ({ title, text, buttonText, showDialog, toggleDialog }) => (
  <div>
    <Dialog open={showDialog} onClose={toggleDialog("showDialog", false)}>
      <DialogTitle>
        <FormattedMessage id={title} />
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <FormattedHTMLMessage id={text} />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={toggleDialog("showDialog", false)}
          color="primary"
          autoFocus
        >
          <FormattedMessage id={buttonText} />
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);
// PropTypes
AlertDialog.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  showDialog: PropTypes.bool.isRequired,
  toggleDialog: PropTypes.func.isRequired
};

export default AlertDialog;
