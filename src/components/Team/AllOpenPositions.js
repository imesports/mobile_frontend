// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import Typography from "material-ui/Typography";
import Card from "material-ui/Card";
// IMPORT components
import { FormInputSpacing } from "components/UI/Wrapper";
import CardContent from "components/UI/Card/Content";
// IMPORT from same folder
import TeamVacancyRoleCard from "./TeamVacancyRoleCard";
// Custom Styles
const CardWrapper = styled(Card)`
  margin-bottom: ${props => props.theme.spacing.small}px;
`;
// Component: Stateless
const TeamAllOpenPositions = props => {
  const rowLength = props.openPositions.vacancies.length;
  return (
    <CardWrapper>
      <CardContent>
        <Typography variant="subheading">
          <FormattedMessage id={"search.teams-open-positions"} />
        </Typography>

        <Typography variant="body1">
          {props.openPositions.size}{" "}
          <FormattedMessage id={"search.positions-found"} />
        </Typography>

        <FormInputSpacing />
        {props.openPositions.vacancies.map((role, index) => (
          <div key={index}>
            <TeamVacancyRoleCard data={role} />
            {rowLength !== index + 1 && <FormInputSpacing />}
          </div>
        ))}
      </CardContent>
    </CardWrapper>
  );
};

export default TeamAllOpenPositions;
