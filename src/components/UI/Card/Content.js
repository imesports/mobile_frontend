import styled from "styled-components";

const CardContent = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px;
  }
`;

export default CardContent;
