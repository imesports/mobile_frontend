// Import constants
import CONSTANTS from "appConstants";
import * as types from "./actionTypes";

// Get token from localStorage
const token = localStorage.getItem(CONSTANTS.AUTH_TOKEN);

const initialState = {
  profile: {},
  games: [],
  teams: [],
  token: token || ""
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case types.UPDATE_LOGGED_IN_PROFILE:
      const { profile } = state;
      const updatedProfile = { ...profile, ...action.profile };
      return { ...state, profile: updatedProfile };

    case types.UPDATE_PLAYER_GAMES:
      return { ...state, games: action.games };

    case types.UPDATE_PLAYER_TEAMS:
      return { ...state, teams: action.teams };

    case types.ADD_PLAYER_TEAM:
      return { ...state, teams: [action.team, ...state.teams] };

    case types.UPDATE_TOKEN:
      return { ...state, token: action.token };

    case types.LOGOUT:
      const logoutUserState = { ...initialState, token: "" };
      return { ...state, ...logoutUserState };

    default:
      return state;
  }
}

export default userReducer;
