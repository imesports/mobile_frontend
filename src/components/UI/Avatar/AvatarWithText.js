// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import { ListItem, ListItemText } from "material-ui/List";
// IMPORT icons
import LinkedinBoxIcon from "mdi-react/LinkedinBoxIcon";
// IMPORT components
import IconButtonOutsideLink from "components/UI/Button/IconButtonOutsideLink";
import CustomSizeCircleAvatar from "./CustomSizeCircle";
// Component: Stateless
const AvatarWithText = ({
  listItemTitle,
  listItemSubTitle,
  listItemImage,
  linkedInUrl
}) => (
  <ListItem>
    <CustomSizeCircleAvatar size="xl" src={listItemImage} />
    <ListItemText
      primary={<FormattedMessage id={listItemTitle} />}
      secondary={<FormattedMessage id={listItemSubTitle} />}
    />
    {linkedInUrl && (
      <IconButtonOutsideLink
        secondaryText
        link={linkedInUrl}
        icon={LinkedinBoxIcon}
        iconSize="large"
      />
    )}
  </ListItem>
);

// PropTypes
AvatarWithText.propTypes = {
  listItemTitle: PropTypes.string.isRequired,
  listItemSubTitle: PropTypes.string.isRequired,
  listItemImage: PropTypes.string.isRequired,
  linkedInUrl: PropTypes.string
};

export default AvatarWithText;
