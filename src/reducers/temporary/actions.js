// Imports constants
import * as types from "./actionTypes";

// Import functions
import { basicTeamInfo } from "util/team";

export const updateTemporaryTeamInfo = teamInfo => ({
  type: types.UPDATE_TEMPORARY_TEAM_INFO,
  teamInfo
});

export const teamInfoLoading = teamInfoLoading => ({
  type: types.TEAM_INFO_LOADING,
  teamInfoLoading
});

// GET TEAM INFO AND SAVE IN REDUX
export const getTeamInfo = (token, teamId) => dispatch => {
  dispatch(updateTemporaryTeamInfo({}));
  dispatch(teamInfoLoading(true));

  basicTeamInfo(token, teamId).then(res => {
    dispatch(teamInfoLoading(false));
    if (res.ok) {
      res.json().then(teamInfo => {
        dispatch(updateTemporaryTeamInfo(teamInfo));
      });
    }
    //TODO: treatment of error
  });
};
