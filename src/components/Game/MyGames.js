// IMPORT libs
import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
// IMPORT material
import { CircularProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
// IMPORT from same folder
import GameCard from "./Card/Game";
// Component: Stateless
const MyGames = props => {
  const { playerGames, gameListLoading } = props;

  return (
    <div>
      {gameListLoading ? (
        <div style={{ textAlign: "center" }}>
          <CircularProgress />
        </div>
      ) : (
        <div>
          {playerGames.length > 0 ? (
            playerGames.map(content => (
              <GameCard
                key={`MyGamesKey ${content.name}`}
                gameId={content.id}
                cardImage={content.background_image}
                title={content.name}
                subtitle="last-updated"
                subtitleValue="x h"
                statsName=""
                stats=""
                statsDiff=""
                buttonText="view-more"
                buttonGoTo={`/my-games/${content.name}`}
              />
            ))
          ) : (
            <div style={{ textAlign: "center" }}>
              <Typography variant="body1" align="center">
                <FormattedMessage id="games.you-dont-have-a-game" />
              </Typography>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

const mapStateToProps = state => ({
  gameListLoading: state.app.gameListLoading,
  playerGames: state.user.games
});

export default connect(mapStateToProps)(MyGames);
