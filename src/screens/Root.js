/**
  Add all screens in the file, add it's route by choosing a proper URL for the container

  keywords: Router, Route, NewRoute, URL, Path
*/
/* Imports from libraries */
import React, { Component } from "react";
import { Route, Switch } from "react-router";
import { ThemeProvider } from "styled-components";
import { MuiThemeProvider } from "material-ui/styles";
import CssBaseline from "material-ui/CssBaseline";

/* Imports Global */
import CONSTANTS from "appConstants";
import { MaterialTheme } from "themes/materialtheme";
import { mainTheme } from "themes";
/* Import Components */
import PrivateRoute from "components/Route/PrivateRoute";
import PublicRoute from "components/Route/PublicRoute";
import asyncComp from "components/AsyncComponent";
import AppHeader from "components/AppHeader";

/* Import Routes */
import RoutesMyTeams from "screens/MyTeams/Routes";
import RoutesMyGames from "screens/MyGames/Routes";
import RoutesPlayer from "screens/Player/Routes";
import RoutesTeam from "screens/Team/Routes";

/* Import Screens */
import ScreenLandingPage from "./LandingPage";
const ScreenRecoverPassword = asyncComp(() => import("./Password/Recover"));
const ScreenSignUpLastStep = asyncComp(() => import("./SignUp/LastStep"));
const ScreenResetPassword = asyncComp(() => import("./Password/Reset"));
const ScreenChampionships = asyncComp(() => import("./Championships"));
const ScreenNotFound = asyncComp(() => import("./NotFound"));
const ScreenSignUp = asyncComp(() => import("./SignUp"));
const ScreenSearch = asyncComp(() => import("./Search"));
const ScreenLogin = asyncComp(() => import("./Login"));
const ScreenHome = asyncComp(() => import("./Home"));

class Root extends Component {
  render() {
    let theme_color = localStorage.getItem(CONSTANTS.THEME_COLOR);
    // Check if theme_color is "light" or "dark", if any other, go back to default "light"
    theme_color =
      theme_color !== "light" && theme_color !== "dark" ? "light" : theme_color;
    return (
      <MuiThemeProvider theme={MaterialTheme({ type: theme_color })}>
        <ThemeProvider theme={mainTheme({ type: theme_color })}>
          <div>
            <CssBaseline />
            {/* Equivalent as normalize.css */}
            <AppHeader appComponent={this} />
            <div>
              <Switch>
                {/* PUBLIC ROUTES */}
                <PublicRoute exact path="/" component={ScreenLandingPage} />
                <PublicRoute exact path="/login" component={ScreenLogin} />
                <PublicRoute exact path="/signup" component={ScreenSignUp} />
                <PublicRoute
                  exact
                  path="/password/recover"
                  component={ScreenRecoverPassword}
                />
                <PublicRoute
                  exact
                  path="/password/reset"
                  component={ScreenResetPassword}
                />

                {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
                <PrivateRoute
                  exact
                  path="/signup/last-step"
                  component={ScreenSignUpLastStep}
                />

                <PrivateRoute exact path="/home" component={ScreenHome} />
                <Route path="/player" component={RoutesPlayer} />
                <Route
                  path="/my-teams"
                  render={props => (
                    <RoutesMyTeams {...props} appComponent={this} />
                  )}
                />
                <Route
                  path="/team/:team_id"
                  render={props => (
                    <RoutesTeam {...props} appComponent={this} />
                  )}
                />
                <Route
                  path="/my-games"
                  render={props => (
                    <RoutesMyGames {...props} appComponent={this} />
                  )}
                />
                <PrivateRoute
                  exact
                  path="/championships"
                  component={ScreenChampionships}
                />
                <PrivateRoute exact path="/search" component={ScreenSearch} />

                {/* UNMATCHED ROUTES - UPDATE */}
                <Route component={ScreenNotFound} />
              </Switch>
            </div>
          </div>
        </ThemeProvider>
      </MuiThemeProvider>
    );
  }
}

export default Root;
