import styled from "styled-components";

const WithFABWrapper = styled.div`
   {
    padding-bottom: calc(
      ${props => props.theme.fabSize.default}px +
        ${props => props.theme.spacing.large}px +
        ${props => props.theme.spacing.small}px
    );
  }
`;

export default WithFABWrapper;
