import styled from "styled-components";

const ActionButtonsWrapper = styled.div`
  && {
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    flex-wrap: wrap;
  }
`;

export default ActionButtonsWrapper;
