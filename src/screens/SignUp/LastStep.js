// IMPORT libs
import React, { Component } from "react";
import { connect } from "react-redux";
// IMPORT actions
import { logoutUser, savePlayerProfile } from "reducers/user/actions";
// IMPORT components
import AppHeaderCloseSave from "components/AppHeader/CloseOrBackSaveOrEdit";
import SignUpLastStepForm from "components/LoginOrSignUp/LastStep";
import { CloseSaveHeaderWrapper } from "components/UI/Wrapper";
// Screen: stateful
class ScreenLastStep extends Component {
  constructor(props) {
    super(props);

    this.state = {
      background_image: props.userProfile.background_image || null,
      avatar: props.userProfile.avatar || null,
      first_name: props.userProfile.first_name || "",
      display_name: props.userProfile.display_name || "",
      last_name: props.userProfile.last_name || "",
      headline: props.userProfile.headline || "",
      location: props.userProfile.location || ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      !nextProps.isLoading &&
      this.props.userProfile !== nextProps.userProfile
    ) {
      this.setState({
        background_image: nextProps.userProfile.background_image || null,
        avatar: nextProps.userProfile.avatar || null,
        first_name: nextProps.userProfile.first_name || "",
        display_name: nextProps.userProfile.display_name || "",
        last_name: nextProps.userProfile.last_name || "",
        headline: nextProps.userProfile.headline || "",
        location: nextProps.userProfile.location || ""
      });
    }
  }

  render() {
    return (
      <CloseSaveHeaderWrapper>
        <AppHeaderCloseSave
          isLoading={this.props.isLoading}
          closeIcon
          closeIconAction={this.props.logout}
          saveButton
          saveAction={this.savePlayerProfile}
          formattedTitle="say-your-name"
        />
        <SignUpLastStepForm
          {...this.state}
          handleChange={this.handleChange}
          isLoading={this.props.isLoading}
        />
      </CloseSaveHeaderWrapper>
    );
  }

  handleChange = (value, prop) => {
    this.setState({ [prop]: value });
  };

  savePlayerProfile = () => {
    var formData = new FormData();
    const body = this.state;
    const { token, userProfile, save } = this.props;

    // Only append to FormData the inputs that changed
    if (body.first_name !== userProfile.first_name)
      formData.append("first_name", body.first_name);
    if (body.last_name !== userProfile.last_name)
      formData.append("last_name", body.last_name);
    if (body.display_name !== userProfile.display_name)
      formData.append("display_name", body.display_name);
    if (body.headline !== userProfile.headline)
      formData.append("headline", body.headline);
    if (body.avatar !== userProfile.avatar && body.avatar !== null)
      formData.append("avatar", body.avatar);
    if (
      body.background_image !== userProfile.background_image &&
      body.background_image !== null
    )
      formData.append("background_image", body.background_image);
    if (body.location !== userProfile.location)
      formData.append("location", body.location);

    save(token, formData, "/home");
  };
}
// Redux Store / State
const mapStateToProps = state => ({
  userProfile: state.user.profile,
  token: state.user.token,
  isLoading: state.app.is_loading
});
// Redux Dispatch
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutUser()),
  save: (user, body, goTo) => dispatch(savePlayerProfile(user, body, goTo))
});

export default connect(mapStateToProps, mapDispatchToProps)(ScreenLastStep);
