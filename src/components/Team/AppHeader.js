/* IMPORT libs */
import React from "react";
import { withRouter } from "react-router";
import { injectIntl } from "react-intl";
import PropTypes from "prop-types";
import { compose } from "redux";
/* IMPORT components */
import TitleBar from "components/UI/Toolbar/CloseOrBackSaveOrEdit";
import { WithLabelTabs, WithLabelTab } from "components/UI/Tab";
import { ThemedAppBar } from "components/UI/AppBar";
/* Stateless Component */
const TeamHeader = props => {
  const { tabs, name, id, intl, history } = props;
  const { pathname } = props.location;

  const handleChange = (event, value) => {
    value === 0
      ? history.push(`/team/${id}`)
      : history.push(`/team/${id}/${tabs[value].value}`);
  };

  const getSelectedTabIndex = (pathname, tabs) => {
    const selectedTab = tabs.filter(
      tab => pathname.toLowerCase() === `/team/${id}/${tab.value}`
    );
    return tabs.indexOf(selectedTab[0]) === -1
      ? 0
      : tabs.indexOf(selectedTab[0]);
  };

  const selectedTab = getSelectedTabIndex(pathname, tabs);

  return (
    <div>
      <ThemedAppBar color="default">
        <TitleBar
          closeIcon
          closeIconAction={() => history.push("/home")}
          title={name}
        />
        <WithLabelTabs
          value={selectedTab}
          onChange={handleChange}
          indicatorColor="primary"
          scrollable
          fullWidth
        >
          {tabs.map(tab => (
            <WithLabelTab
              key={`TeamHeaderTabs ${tab.name}`}
              label={intl.formatMessage({ id: tab.name })}
              icon={<tab.icon />}
            />
          ))}
        </WithLabelTabs>
      </ThemedAppBar>
      {props.children}
    </div>
  );
};
/* PropTypes */
TeamHeader.propTypes = {
  tabs: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired
};

export default compose(withRouter, injectIntl)(TeamHeader);
