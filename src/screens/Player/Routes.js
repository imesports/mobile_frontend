/* Imports from libraries */
import React from "react";
import { Switch } from "react-router";
/* Import Components */
import PrivateRoute from "components/Route/PrivateRoute";
import asyncComp from "components/AsyncComponent";
import AppHeaderBack from "components/AppHeader/CloseOrBackSaveOrEdit";
/* Import Screens */
const ScreenPlayerProfile = asyncComp(() => import("screens/Player/Profile"));
// All /player/ routes
const PlayerRoute = props => (
  <div>
    {/* Equivalent as normalize.css */}
    <AppHeaderBack backIcon title="Player" />
    <div>
      <Switch>
        {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
        <PrivateRoute
          path={`${props.match.url}/:player_id`}
          component={ScreenPlayerProfile}
        />
      </Switch>
    </div>
  </div>
);

export default PlayerRoute;
