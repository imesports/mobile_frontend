import React from "react";

import { LoggedInWrapper } from "components/UI/Wrapper";

const TeamProfileTraining = props => {
  return (
    <LoggedInWrapper>
      <div>Profile Training</div>
    </LoggedInWrapper>
  );
};

export default TeamProfileTraining;
