import React from "react";
import ReactDOM from "react-dom";
import CONSTANTS from "appConstants";

// Redux implementation
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, combineReducers } from "redux";
import createHistory from "history/createBrowserHistory";
import {
  ConnectedRouter,
  routerReducer,
  routerMiddleware
} from "react-router-redux";
import reducers from "./reducers";

// Service Worker implementation
import registerServiceWorker from "./registerServiceWorker";
import Root from "./screens";
import "./index.css";

// i18n integration
import LanguageProvider from "providers/LanguageProvider";
import { translationMessages } from "providers/i18n";

// Apollo implementation
import { ApolloLink } from "apollo-client-preset";
import { onError } from "apollo-link-error";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

// Apollo Error Handler
const errorLink = onError(({ networkError, graphQLErrors }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      // alert(`Message: ${message}`)
      console.log(
        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
      )
    );
  }
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const httpLink = new HttpLink({ uri: CONSTANTS.BACKEND_URL });
// Apollo middleware for JWT authentication
const middlewareAuthLink = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem(CONSTANTS.AUTH_TOKEN);
  const authorizationHeader = token ? `JWT ${token}` : null;
  operation.setContext({
    headers: {
      authorization: authorizationHeader
    }
  });
  return forward(operation);
});

// const httpLinkWithAuthToken = middlewareAuthLink.concat(httpLink);
const finalLink = ApolloLink.from([middlewareAuthLink, errorLink, httpLink]);

const client = new ApolloClient({
  link: finalLink,
  cache: new InMemoryCache()
});

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = [routerMiddleware(history), thunk];
const reducer = combineReducers({
  ...reducers,
  router: routerReducer
});

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(...middleware)
);

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      {/* LanguageProvider add i18n integration with the application */}
      <LanguageProvider messages={translationMessages}>
        {/* ConnectedRouter will use the store from Provider automatically */}
        <ConnectedRouter history={history}>
          <Root />
        </ConnectedRouter>
      </LanguageProvider>
    </Provider>
  </ApolloProvider>,
  document.getElementById("root")
);
registerServiceWorker();
