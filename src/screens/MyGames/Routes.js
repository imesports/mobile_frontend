/* Imports from libraries */
import React from "react";
import { Switch } from "react-router";
/* Import Components */
import PrivateRoute from "components/Route/PrivateRoute";
import asyncComp from "components/AsyncComponent";
/* Import Screens */
const ScreenMyGamesAdd = asyncComp(() => import("screens/MyGames/Add"));
const ScreenMyGames = asyncComp(() => import("screens/MyGames"));
// All /my-games routes
const MyGamesRoutes = props => (
  <div>
    <div>
      <Switch>
        {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
        <PrivateRoute exact path={props.match.url} component={ScreenMyGames} />
        <PrivateRoute
          exact
          path={`${props.match.url}/add`}
          component={ScreenMyGamesAdd}
        />
      </Switch>
    </div>
  </div>
);

export default MyGamesRoutes;
