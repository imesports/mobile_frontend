// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT fetch
import { createTeamFetch } from "util/team";
// IMPORT actions
import { getPlayerGames, addPlayerTeam } from "reducers/user/actions";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
import Button from "material-ui/Button";
// IMPORT icons
import ChevronRightIcon from "mdi-react/ChevronRightIcon";
import ChevronLeftIcon from "mdi-react/ChevronLeftIcon";
// IMPORT components
import CircularProgress from "components/UI/Progress/Circular";
import { NoButtonIcon } from "components/UI/IconFormatter";
import CardContent from "components/UI/Card/Content";
// IMPORT from same folder
import {
  StepperContainerWrapper,
  StyledMobileStepper,
  SpacingPattern,
  StepperPosition
} from "components/UI/Wrapper/Stepper";
import CreateTeamForm from "./Form";

// Component: Stateful
class CreateTeamStepper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      avatar: null,
      name: "",
      tag: "",
      game: "",
      location: "",
      language: "",
      objectives: "",
      localLoading: false
    };
  }
  render() {
    const { userId, token, playerGames } = this.props;
    const { localLoading, activeStep } = this.state;

    return (
      <div>
        <StepperContainerWrapper>
          {localLoading && <LinearProgress color="secondary" />}
          <CardContent>
            {userId && token ? (
              <CreateTeamForm
                {...this.state}
                loading={localLoading}
                handleChange={this.handleChange}
                token={token}
                userId={userId}
                playerGames={playerGames}
                verifyUserHasGame={this.verifyUserHasGame}
              />
            ) : (
              <CircularProgress />
            )}
          </CardContent>
        </StepperContainerWrapper>
        <StepperPosition>
          <StyledMobileStepper
            variant="dots"
            steps={2}
            position="static"
            activeStep={activeStep}
            nextButton={
              <SpacingPattern>
                {activeStep === 1 ? (
                  <Button
                    style={{ float: "right" }}
                    color="primary"
                    onClick={this.createTeam}
                    disabled={(!userId && !token) || localLoading}
                  >
                    <FormattedMessage id="done" />
                  </Button>
                ) : (
                  <Button
                    style={{ float: "right" }}
                    color="primary"
                    onClick={this.handleNext}
                  >
                    <FormattedMessage id="next" />
                    <NoButtonIcon
                      fill={localLoading ? "default" : "primary"}
                      size="sm"
                    >
                      <ChevronRightIcon />
                    </NoButtonIcon>
                  </Button>
                )}
              </SpacingPattern>
            }
            backButton={
              <SpacingPattern>
                {activeStep === 1 && (
                  <Button
                    color="primary"
                    onClick={this.handleBack}
                    disabled={activeStep === 0 || localLoading}
                  >
                    <NoButtonIcon
                      fill={localLoading ? "default" : "primary"}
                      size="sm"
                    >
                      <ChevronLeftIcon />
                    </NoButtonIcon>
                    <FormattedMessage id="back" />
                  </Button>
                )}
              </SpacingPattern>
            }
          />
        </StepperPosition>
      </div>
    );
  }

  handleNext = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1
    }));
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1
    }));
  };

  handleChange = (state, value) => {
    this.setState({ [state]: value });
  };

  createTeam = () => {
    this.setState({ localLoading: true });
    let formData = new FormData();
    const {
      avatar,
      name,
      tag,
      game,
      location,
      language,
      objectives
    } = this.state;

    const { token, addTeam } = this.props;

    avatar && formData.append("avatar", avatar);
    name && formData.append("name", name);
    tag && formData.append("tag", tag);
    game && formData.append("game", game);
    location && formData.append("location", location);
    language && formData.append("language", language);
    objectives && formData.append("objectives", objectives);

    // QUESTION
    //TODO: Add game if player dont have game yet

    createTeamFetch(token, formData).then(res => {
      this.setState({ localLoading: false });
      if (res.ok) {
        res.json().then(createdTeam => {
          addTeam(createdTeam);
          this.props.history.push(`/team/${createdTeam.id}`);
        });
      } else {
        //Error
      }
    });
  };

  verifyUserHasGame = (gameId, playerGames) => {
    return playerGames.findIndex(i => i.id === gameId) === -1 ? false : true;
  };
}
// PropTypes
CreateTeamStepper.propTypes = {
  teamsList: PropTypes.array,
  userId: PropTypes.string
};
// Redux Store / State
const mapStateToProps = state => ({
  userId: state.user.profile.user,
  playerGames: state.user.games,
  token: state.user.token
});
// Redux dispatch
const mapDispatchToProps = dispatch => ({
  getPlayerGames: token => dispatch(getPlayerGames(token)),
  addTeam: teams => dispatch(addPlayerTeam(teams))
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(CreateTeamStepper);
