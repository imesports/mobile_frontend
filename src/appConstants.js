// Declare all APP level constants
export default {
  DEFAULT_LOCALE: "en-US",
  DEFAULT_BACKGROUND_IMAGE: "https://i.imgur.com/9od0mSo.jpg",
  DEFAULT_AVATAR_IMAGE:
    "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_1280.png",
  BACKEND_URL: "https://api-next.imesports.gg",
  AUTH_TOKEN: "auth-token",
  THEME_COLOR: "theme-color"
};
