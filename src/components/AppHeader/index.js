// IMPORT libs
import React from "react";
import { connect } from "react-redux";
// IMPORT util/helpers
import tabs from "util/helpers/loggedInTabs";
// IMPORT components
import LoggedIn from "./LoggedIn";
import Public from "./Public";

const Header = props => {
  const { appComponent } = props;
  const { pathname } = props.router.location;

  if (
    pathname === "/" ||
    pathname === "/login" ||
    pathname === "/login/" ||
    pathname === "/signup" ||
    pathname === "/signup/" ||
    pathname === "/password/recover" ||
    pathname === "/password/recover/" ||
    pathname === "/password/reset" ||
    pathname === "/password/reset/"
  ) {
    return <Public appComponent={appComponent} backButton={pathname !== "/"} />;
  }

  if (
    pathname === "/championships" ||
    pathname === "/championships/" ||
    pathname === "/my-teams" ||
    pathname === "/my-teams/" ||
    pathname === "/search" ||
    pathname === "/search/" ||
    pathname === "/home" ||
    pathname === "/home/" ||
    pathname === "/my-games" ||
    pathname === "/my-games/"
  ) {
    return (
      <LoggedIn appComponent={appComponent} pathname={pathname} tabs={tabs} />
    );
  }
  return null;
};
// Redux Store / State
const mapStateToProps = state => ({
  router: state.router
});

export default connect(mapStateToProps)(Header);
