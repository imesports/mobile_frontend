// IMPORT libs
import React, { Component } from "react";
import PropTypes from "prop-types";
// IMPORT material
import Card from "material-ui/Card";
// IMPORT components
import CardWrapper from "components/UI/Wrapper/Card";
// IMPORT from same folder
import TopProfileHeader from "./Header";
// IMPORT images
import DefaultProfileAvatar from "imgs/default/profile_avatar.jpg";
import DefaultProfileCover from "imgs/default/profile_cover.jpg";
// Component: Stateful
class TopProfileCard extends Component {
  state = {
    avatar: DefaultProfileAvatar,
    background_image: DefaultProfileCover,
    first_name: "",
    display_name: "",
    last_name: "",
    headline: "",
    location: "",
    ownerProfile: false
  };

  render() {
    const { userData } = this.props;
    const fillData = userData || this.state;
    return (
      <CardWrapper>
        <Card>
          <TopProfileHeader
            avatar={fillData.avatar}
            background_image={fillData.background_image}
            first_name={fillData.first_name}
            display_name={fillData.display_name}
            last_name={fillData.last_name}
            headline={fillData.headline}
            location={fillData.location}
            editProfileOption={fillData.ownerProfile}
          />
        </Card>
      </CardWrapper>
    );
  }
}
// PropTypes
TopProfileCard.propTypes = {
  userData: PropTypes.object
};

export default TopProfileCard;
