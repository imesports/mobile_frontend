// IMPORT libs
import React from "react";
import { FormattedHTMLMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT components
import { LogoImg } from "components/UI/Img";
// Custom Styles
const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: ${props => props.theme.spacing.default}px;
`;

const NoContentText = styled(Typography)`
  && {
    color: ${props => props.theme.text.secondary};
    margin-bottom: ${props => props.theme.spacing.xs}px;
  }
`;
// Component: Stateless
const NoContent = ({ title, subheading, img, callToAction }) => (
  <ContentWrapper>
    <NoContentText variant="title">
      <FormattedHTMLMessage id={title} />
    </NoContentText>
    {subheading && (
      <NoContentText variant="subheading">
        <FormattedHTMLMessage id={subheading} />
      </NoContentText>
    )}
    <LogoImg src={img} />
    <NoContentText variant="subheading">
      <FormattedHTMLMessage id={callToAction} />
    </NoContentText>
  </ContentWrapper>
);

export default NoContent;
