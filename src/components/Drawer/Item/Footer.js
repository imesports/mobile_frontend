// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// Custom Styles
const MenuItemWrapper = styled.div`
   {
    display: flex;
    width: 100%;
    align-items: center;

    padding: ${props => props.theme.spacing.small}px
      ${props => props.theme.spacing.default}px;

    :hover {
      cursor: pointer;
      background-color: ${props => props.theme.background.hover};
    }
  }
`;
// Component: Stateless
const FooterDrawerItem = ({
  goTo,
  formattedText,
  externalLink = false,
  text
}) => {
  const innerContent = () => (
    <MenuItemWrapper>
      <Typography>
        {formattedText && <FormattedMessage id={formattedText} />}
        {text}
      </Typography>
    </MenuItemWrapper>
  );
  return (
    <div>
      {externalLink ? (
        <a href={goTo} target="_blank" rel="noopener noreferrer">
          {innerContent()}
        </a>
      ) : (
        <Link to={goTo}>{innerContent()}</Link>
      )}
    </div>
  );
};
// PropTypes
FooterDrawerItem.propTypes = {
  goTo: PropTypes.string.isRequired,
  formattedText: PropTypes.string,
  externalLink: PropTypes.bool,
  text: PropTypes.string
};

export default FooterDrawerItem;
