// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Card, { CardMedia } from "material-ui/Card";
import Typography from "material-ui/Typography";
// IMPORT components
import ActionButtonsWrapper from "components/UI/Wrapper/ActionButtons";
import ActionButton from "components/UI/Button/ActionButton";
import { DividerDefaultMargin } from "components/UI/Divider";
import CardWrapper from "components/UI/Wrapper/Card";
// Custom Styles
const StyledCard = styled(Card)`
  && {
    display: flex;
  }
`;

const StyledCardMedia = styled(CardMedia)`
  && {
    min-width: ${props => props.theme.cardMediaSize.medium}px;
  }
`;

const StyledCardContent = styled.div`
  && {
    padding: ${props => props.theme.spacing.default}px;
    flex-direction: column;
    flex: auto;
  }
`;
// Component: Stateless
const CallToActionCard = ({
  cardImage,
  title,
  subtitle,
  dismissText,
  callToActionText,
  dismissAction,
  callToActionAction,
  intl
}) => (
  <CardWrapper>
    <StyledCard>
      <StyledCardMedia
        image={cardImage}
        title={intl.formatMessage({ id: title })}
      />
      <StyledCardContent>
        <Typography variant="headline">
          <FormattedMessage id={title} />
        </Typography>
        {subtitle && (
          <Typography>
            <FormattedMessage id={subtitle} />
          </Typography>
        )}
        <DividerDefaultMargin />
        <ActionButtonsWrapper>
          <ActionButton size="small" color="primary" onClick={dismissAction}>
            <FormattedMessage id={dismissText} />
          </ActionButton>
          <ActionButton
            size="small"
            color="primary"
            onClick={callToActionAction}
          >
            <FormattedMessage id={callToActionText} />
          </ActionButton>
        </ActionButtonsWrapper>
      </StyledCardContent>
    </StyledCard>
  </CardWrapper>
);
// PropTypes
CallToActionCard.propTypes = {
  cardImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  dismissText: PropTypes.string.isRequired,
  callToActionText: PropTypes.string.isRequired,
  dismissAction: PropTypes.func.isRequired,
  callToActionAction: PropTypes.func.isRequired
};

export default injectIntl(CallToActionCard);
