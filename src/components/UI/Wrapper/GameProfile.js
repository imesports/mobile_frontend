import styled from "styled-components";

export const FirstRow = styled.div`
   {
    display: flex;
    align-items: center;
  }
`;

export const SecondRow = styled.div`
   {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

export const StayTogetherWrapper = styled.div`
   {
    display: flex;
    align-items: center;
  }
  div:first-child {
    margin-right: ${props => props.theme.spacing.xs}px;
  }
`;

export const AvatarPosition = styled.div`
  position: absolute;
  bottom: calc(-1 * (${props => props.theme.iconSize.xl}px / 2));
  left: ${props => props.theme.spacing.default}px;
`;
