import styled from "styled-components";

import Button from "material-ui/Button";

const ActionButton = styled(Button)`
  && {
    color: ${props => props.theme.primary.default};
    min-width: auto;
  }
`;

export default ActionButton;
