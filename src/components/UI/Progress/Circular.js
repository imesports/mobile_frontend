import React from "react";
import styled from "styled-components";

import { CircularProgress } from "material-ui/Progress";

const CentralizeProgress = styled.div`
   {
    width: 100%;
    display: flex;
    justify-content: center;
  }
`;

const CentralizedCircularProgress = () => (
  <CentralizeProgress>
    <CircularProgress />
  </CentralizeProgress>
);

export default CentralizedCircularProgress;
