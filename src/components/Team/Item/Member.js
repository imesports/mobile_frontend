// IMPORT libs
import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT global constants
import CONSTANTS from "appConstants";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
// STYLES
const Item = styled.div`
   {
    display: flex;
    align-items: center;
    width: 100%;
  }
`;

const ItemText = styled.div`
   {
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: calc(100% - ${props => props.theme.iconSize.xl}px);
    padding-left: ${props => props.theme.spacing.small}px;
  }
`;
// Stateless component
const MemberItem = props => {
  const {
    url_handle,
    first_name,
    display_name,
    last_name,
    gameRole,
    avatar
  } = props;

  return (
    <Link to={`/player/${url_handle}`}>
      <Item>
        <CustomSizeCircleAvatar
          src={avatar || CONSTANTS.DEFAULT_AVATAR_IMAGE}
          size="xl"
        />
        <ItemText>
          <Typography variant="body2" noWrap>
            {first_name} &ldquo;{display_name}&rdquo; {last_name}
          </Typography>
          <Typography variant="caption" noWrap>
            {gameRole}
          </Typography>
        </ItemText>
      </Item>
    </Link>
  );
};
// PropTypes
MemberItem.propTypes = {
  display_name: PropTypes.string.isRequired,
  first_name: PropTypes.string.isRequired,
  url_handle: PropTypes.string.isRequired,
  last_name: PropTypes.string.isRequired,
  gameRole: PropTypes.string,
  avatar: PropTypes.string
};

export default MemberItem;
