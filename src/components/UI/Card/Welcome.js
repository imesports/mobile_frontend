// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Card, { CardMedia } from "material-ui/Card";
import Typography from "material-ui/Typography";
// IMPORT components
import ActionButtonsWrapper from "components/UI/Wrapper/ActionButtons";
import ActionButton from "components/UI/Button/ActionButton";
import { DividerDefaultMargin } from "components/UI/Divider";
import { WhiteTypography } from "components/UI/Typography";
import CardWrapper from "components/UI/Wrapper/Card";
import CardContent from "components/UI/Card/Content";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
  && {
    position: relative;
    height: ${props => props.theme.cardMediaSize.large}px;
  }
`;

const CardMediaText = styled.div`
  &&& {
    position: absolute;
    bottom: 0;
    padding: ${props => props.theme.spacing.default}px;
    width: 100%;
    background: rgba(0, 0, 0, 0.3);
  }
`;
// Component: Stateless
const WelcomeCard = ({
  cardImage,
  title,
  message,
  dismissText,
  callToActionText,
  shareText,
  dismissAction,
  callToActionAction,
  shareAction,
  intl
}) => (
  <CardWrapper>
    <Card>
      <StyledCardMedia
        image={cardImage}
        title={intl.formatMessage({ id: title })}
      >
        <CardMediaText>
          <WhiteTypography variant="headline">
            <FormattedMessage id={title} />
          </WhiteTypography>
        </CardMediaText>
      </StyledCardMedia>
      <CardContent>
        <Typography>
          <FormattedMessage id={message} />
        </Typography>
        <DividerDefaultMargin />
        <ActionButtonsWrapper>
          <ActionButton size="small" color="primary" onClick={dismissAction}>
            <FormattedMessage id={dismissText} />
          </ActionButton>
          <ActionButton
            size="small"
            color="primary"
            onClick={callToActionAction}
          >
            <FormattedMessage id={callToActionText} />
          </ActionButton>
          <ActionButton size="small" color="primary" onClick={shareAction}>
            <FormattedMessage id={shareText} />
          </ActionButton>
        </ActionButtonsWrapper>
      </CardContent>
    </Card>
  </CardWrapper>
);

// PropTypes
WelcomeCard.propTypes = {
  cardImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  dismissText: PropTypes.string.isRequired,
  callToActionText: PropTypes.string.isRequired,
  shareText: PropTypes.string.isRequired,
  dismissAction: PropTypes.func.isRequired,
  callToActionAction: PropTypes.func.isRequired,
  shareAction: PropTypes.func.isRequired
};

export default injectIntl(WelcomeCard);
