import styled from "styled-components";

import Typography from "material-ui/Typography";

export const NameTypography = styled(Typography)`
  && {
    color: ${props => props.theme.text.primary};
  }
`;

export const TitleTypography = styled(Typography)`
  && {
    color: ${props => props.theme.text.primary};
    margin: ${props => props.theme.spacing.xs}px 0px;
  }
`;
