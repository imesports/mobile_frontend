// IMPORT libs
import styled from "styled-components";
// IMPORT material
import Button from "material-ui/Button";
import Card from "material-ui/Card";

export const StyledButton = styled(Button)`
  && {
    position: fixed;
    bottom: ${props => props.theme.spacing.large}px;
    right: ${props => props.theme.spacing.default}px;
    fill: ${props => props.theme.icon.fabIcon};
  }
`;

export const Menu = styled.div`
   {
    display: flex;
    flex-direction: column;

    position: fixed;
    bottom: calc(
      ${props => props.theme.spacing.large}px +
        ${props => props.theme.fabSize.default}px
    );
    right: calc(
      ${props => props.theme.spacing.default}px +
        ${props => props.theme.fabSize.default}px / 2 -
        ${props => props.theme.fabSize.mini}px / 2
    );
    fill: ${props => props.theme.icon.fabIcon};

    visibility: ${props => (props.show ? "visible" : "hidden")};
    opacity: ${props => (props.show ? "1" : "0")};
    transition: opacity 0.2s, visibility 0.2s;
  }
`;

export const MenuItem = styled.div`
   {
    display: flex;
    justify-content: flex-end;
    align-items: center;

    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const StyledCard = styled(Card)`
  && {
    margin-right: ${props => props.theme.spacing.small}px;
    padding: ${props => props.theme.spacing.xxxs}px
      ${props => props.theme.spacing.xs}px;
  }
`;

export const FABIcon = styled.div`
   {
    transition: 0.2s;
    transform: ${props => props.show && "rotate(45deg)"};
    display: flex;
    justify-content: center;
    fill: ${props =>
      props.fill ? props.theme.icon.default : props.theme.icon.fill};
  }
`;

export const BlurBackground = styled.div`
   {
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;

    background-color: ${props => props.theme.background.body}C8;

    visibility: ${props => (props.show ? "visible" : "hidden")};
    opacity: ${props => (props.show ? "1" : "0")};
    transition: opacity 0.2s, visibility 0.2s;
  }
`;
