// IMPORT libs
import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import { CardMedia } from "material-ui/Card";
// IMPORT constants
import CONSTANTS from "appConstants";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { WhiteTypography } from "components/UI/Typography";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
   {
    min-height: ${props => props.theme.cardMediaSize.large}px;
    position: relative;
  }
`;

const AvatarPosition = styled.div`
   {
    position: absolute;
    top: ${props => props.theme.spacing.large}px;
    left: ${props => props.theme.spacing.default}px;
  }
`;

const NamePosition = styled.div`
  &&& {
    position: absolute;
    bottom: 0px;
    padding: ${props => props.theme.spacing.small}px
      ${props => props.theme.spacing.default}px;
    width: 100%;
    background-color: rgba(0, 0, 0, 0.3);
  }
`;
// Component: Stateless
const DrawerHeader = ({
  background_image = CONSTANTS.default_backgorund_img,
  avatar = CONSTANTS.default_avatar_img,
  first_name,
  last_name,
  display_name,
  email
}) => (
  <StyledCardMedia image={background_image}>
    <AvatarPosition>
      <CustomSizeCircleAvatar size="xl" src={avatar} />
    </AvatarPosition>
    <NamePosition>
      <WhiteTypography>
        <b>
          {first_name}{" "}
          {display_name && <span>&ldquo;{display_name}&rdquo;</span>}{" "}
          {last_name}
        </b>
      </WhiteTypography>
      <WhiteTypography variant="caption">{email}</WhiteTypography>
    </NamePosition>
  </StyledCardMedia>
);

// PropTypes
DrawerHeader.propTypes = {
  background_image: PropTypes.string,
  avatar: PropTypes.string,
  email: PropTypes.string,
  first_name: PropTypes.string,
  last_name: PropTypes.string,
  display_name: PropTypes.string
};

export default DrawerHeader;
