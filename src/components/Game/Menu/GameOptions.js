// IMPORT libs
import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
// IMPORT Redux Actions
import { updatePlayerGames } from "reducers/user/actions";
// IMPORT util / fetch
import { removeGameFetch } from "util/game";
// IMPORT material
import Popover from "material-ui/Popover";
import { MenuItem } from "material-ui/Menu";
// IMPORT components
import DialogConfirmationOnRemove from "components/Game/Dialog/ConfirmationOnRemove";
//Stateless Component
class GameOptions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      disableRemoveGame: false,
      confirmationOpen: false
    };
  }

  render() {
    const {
      anchorEl,
      closeAction,
      gameId,
      gameAbbreviation,
      userId,
      token
    } = this.props;
    const { disableRemoveGame, confirmationOpen } = this.state;

    return (
      <Popover
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={closeAction}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        margin="small"
      >
        <MenuItem
          onClick={() =>
            disableRemoveGame ? null : this.confirmationToggle(true)
          }
          disabled={disableRemoveGame}
        >
          <FormattedMessage id="remove-game" />
        </MenuItem>
        <DialogConfirmationOnRemove
          open={confirmationOpen}
          closeAction={() => this.confirmationToggle(false)}
          confirmAction={() => this.removeGame(token, userId, gameId)}
          gameAbbreviation={gameAbbreviation}
        />
      </Popover>
    );
  }

  removeGame = (token, user_id, game_id) => {
    this.setState({ confirmationOpen: false });
    this.props.loadingToggle(true);
    this.setState({ disableRemoveGame: true });
    removeGameFetch(token, user_id, game_id).then(response => {
      if (response.ok) {
        response.json().then(res => this.props.updatePlayerGames(res.games));
      } else {
        // Treatment of Error
      }
    });
  };

  confirmationToggle = open => {
    this.setState({ confirmationOpen: open });
    if (!open) this.props.closeAction();
  };
}

GameOptions.propTypes = {
  gameAbbreviation: PropTypes.string.isRequired,
  gameId: PropTypes.number.isRequired,
  anchorEl: PropTypes.object,
  closeAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  token: state.user.token,
  userId: state.user.profile.user
});

const mapDispatchToProps = dispatch => ({
  updatePlayerGames: games => dispatch(updatePlayerGames(games))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameOptions);
