// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
import List from "material-ui/List";
// IMPORT components
import AvatarWithText from "components/UI/Avatar/AvatarWithText";
import { BetweenCardsTextWrapper } from "components/UI/Wrapper";

// Component: Stateless
const LandingPageCircleCardsList = ({ title, cardsList }) => (
  <div>
    <BetweenCardsTextWrapper>
      <Typography>
        <FormattedMessage id={title} />
      </Typography>
    </BetweenCardsTextWrapper>
    <List>
      {cardsList.map(content => (
        <AvatarWithText
          key={content.listItemTitle}
          listItemTitle={content.listItemTitle}
          listItemSubTitle={content.listItemSubTitle}
          listItemImage={content.listItemImage}
          linkedInUrl={content.linkedInUrl}
        />
      ))}
    </List>
  </div>
);

// PropTypes
LandingPageCircleCardsList.propTypes = {
  title: PropTypes.string.isRequired,
  cardsList: PropTypes.array.isRequired
};

export default LandingPageCircleCardsList;
