// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import Typography from "material-ui/Typography";
// Custom Styles
const CardTitle = styled.div`
  margin: ${props => props.theme.spacing.xl}px 0;
`;
// Component: Stateless
const CardHeaderTitleSubtitle = ({ title, subtitle }) => (
  <CardTitle>
    <Typography variant="headline">
      <FormattedMessage id={title} />
    </Typography>
    <Typography variant="body1">
      <FormattedHTMLMessage id={subtitle} />
    </Typography>
  </CardTitle>
);

export default CardHeaderTitleSubtitle;
