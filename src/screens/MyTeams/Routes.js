/* Imports from libraries */
import React from "react";
import { Switch } from "react-router";
/* Import Components */
import PrivateRoute from "components/Route/PrivateRoute";
import asyncComp from "components/AsyncComponent";
/* Import Screens */
const ScreenMyTeamsCreate = asyncComp(() => import("screens/MyTeams/Create"));
const ScreenMyTeams = asyncComp(() => import("screens/MyTeams"));
// All /my-teams routes
const MyTeamsRoutes = props => (
  <Switch>
    {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
    <PrivateRoute exact path={props.match.url} component={ScreenMyTeams} />
    <PrivateRoute
      exact
      path={`${props.match.url}/create`}
      component={ScreenMyTeamsCreate}
    />
  </Switch>
);

export default MyTeamsRoutes;
