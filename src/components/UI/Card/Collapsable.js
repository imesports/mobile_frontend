/* IMPORT libs */
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
/* IMPORT material */
import Collapse from "material-ui/transitions/Collapse";
import Typography from "material-ui/Typography";
import Card from "material-ui/Card";
/* IMPORT components*/
import { ThickDivider } from "components/UI/Divider";
import CardContent from "components/UI/Card/Content";
/* IMPORT icons */
import ChevronDownIcon from "mdi-react/ChevronDownIcon";
/* IMPORT styles */
import { DefaultIcon } from "components/UI/IconFormatter";
import CardWrapper from "components/UI/Wrapper/Card";
/* Styled Components */
const CardHeader = styled.div`
   {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: ${props => props.theme.spacing.small}px
      ${props => props.theme.spacing.default}px;
  }
`;

const RotateIcon = styled.div`
   {
    transition: 0.2s;
    ${props => props.expanded && "transform: rotate(180deg)"};
  }
`;
/* Stateful component */
class CollapsableCard extends Component {
  state = {
    expanded: true || this.props.expanded
  };

  render() {
    const { title, formattedTitle, children } = this.props;
    const { expanded } = this.state;

    return (
      <CardWrapper>
        <Card>
          <CardHeader onClick={() => this.handleExpanded()}>
            <Typography variant="body2" noWrap>
              {formattedTitle && <FormattedMessage id={formattedTitle} />}
              {title}
            </Typography>
            <RotateIcon expanded={expanded}>
              <DefaultIcon size="sm" fill="contrast">
                <ChevronDownIcon />
              </DefaultIcon>
            </RotateIcon>
          </CardHeader>
          <Collapse in={expanded} timeout="auto">
            <ThickDivider />
            <CardContent>
              {/* Break code if there are 2 elements on children prop */
              React.Children.only(children)}
            </CardContent>
          </Collapse>
        </Card>
      </CardWrapper>
    );
  }
  /* Inner Functions */
  handleExpanded = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  };
}
/* PropTypes */
CollapsableCard.propTypes = {
  title: PropTypes.string,
  formattedTitle: PropTypes.string,
  children: PropTypes.object.isRequired
};

export default CollapsableCard;
