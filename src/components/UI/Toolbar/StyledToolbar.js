import Toolbar from "material-ui/Toolbar";
import styled from "styled-components";

const StyledToolbar = styled(Toolbar)`
  && {
    min-height: ${props => props.theme.headerSize.commonToolbar}px;
    height: ${props => props.theme.headerSize.commonToolbar}px;
    padding: 0px ${props => props.theme.spacing.xs}px;
  }
`;

export default StyledToolbar;
