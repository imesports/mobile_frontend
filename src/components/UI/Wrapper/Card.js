import styled from "styled-components";

const CardWrapper = styled.div`
  margin-bottom: ${props => props.theme.spacing.small}px;
`;

export default CardWrapper;
