// IMPORT Libs
import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ReactSVG from "react-svg";
import { FormattedMessage } from "react-intl";

// IMPORT Components
import IconButtonOutsideLink from "components/UI/Button/IconButtonOutsideLink";
import { SocialNetworksWrapper, LogoWrapper } from "components/UI/Wrapper";
import { LogoImg } from "components/UI/Img";
import Logo from "imgs/logo.svg";
// IMPORT from same folder
import { FooterSubTitle } from "./Typography";

// Custom Styles
export const FooterWrapper = styled.div`
   {
    background: ${props => props.theme.background.footer};
    padding: ${props => props.theme.spacing.xl}px;
    color: ${props => props.theme.text.footer};
    text-align: center;
  }
`;

// Component: Stateless
const LandingPageFooter = ({ subtitle, socialNetworks }) => (
  <FooterWrapper>
    <LogoWrapper>
      <LogoImg color="footer">
        <ReactSVG path={Logo} />
      </LogoImg>
    </LogoWrapper>
    <FooterSubTitle>
      <FormattedMessage id={subtitle} />
    </FooterSubTitle>
    <SocialNetworksWrapper>
      {socialNetworks.map(site => (
        <IconButtonOutsideLink
          key={site.link}
          link={site.link}
          icon={site.icon}
          iconSize="large"
        />
      ))}
    </SocialNetworksWrapper>
  </FooterWrapper>
);

// PropTypes
LandingPageFooter.propTypes = {
  subtitle: PropTypes.string.isRequired,
  socialNetworks: PropTypes.array.isRequired
};

export default LandingPageFooter;
