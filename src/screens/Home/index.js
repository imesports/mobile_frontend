// IMPORT libs
import React from "react";
// IMPORT components
import CallToActionCard from "components/UI/Card/CallToAction";
import { LoggedInWrapper } from "components/UI/Wrapper";
import WelcomeCard from "components/UI/Card/Welcome";
// Screen: Stateless
const ScreenHome = props => (
  <LoggedInWrapper>
    <WelcomeCard
      cardImage="https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX12099251.jpg"
      title="welcome!"
      message="welcome-message"
      dismissText="dismiss"
      callToActionText="watch-tutorial"
      shareText="share"
    />
    <CallToActionCard
      cardImage="https://s3-alpha.figma.com/img/3b29/0066/6a73a30a64666770d16f5278cd74bf41?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJK6APQGEHTP6I3PA%2F20180311%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20180311T223443Z&X-Amz-Expires=60&X-Amz-SignedHeaders=host&X-Amz-Signature=512ad436c8a0bfa3d5885d25573e110487c1cb19a27e59343bc8fadc11f8fed6"
      title="call-to-action.connect-to-steam"
      dismissText="dismiss"
      callToActionText="connect"
    />
    <CallToActionCard
      cardImage="https://s3-alpha.figma.com/img/aabb/bde6/1e09a0dcfeb83480d189bcb5eace7ec8?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJK6APQGEHTP6I3PA%2F20180312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20180312T041627Z&X-Amz-Expires=60&X-Amz-SignedHeaders=host&X-Amz-Signature=5ebca5c57aa56a47a10132f9a8e547e1d0a913c0e3d0eb9ba6179f4c3546a1b3"
      title="call-to-action.connect-to-lol"
      dismissText="dismiss"
      callToActionText="connect"
    />
    <CallToActionCard
      cardImage="https://s3-alpha.figma.com/img/0ad0/6654/6ef42c6114167b00d4a66c043436f8b9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJK6APQGEHTP6I3PA%2F20180312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20180312T041722Z&X-Amz-Expires=60&X-Amz-SignedHeaders=host&X-Amz-Signature=81a65ce21a8c381926f619c6a1d614453a7a1266b3a200d146eb0800e18a1aa0"
      title="call-to-action.connect-to-fortnite"
      dismissText="dismiss"
      callToActionText="connect"
    />
    <CallToActionCard
      cardImage="https://s3-alpha.figma.com/img/28bc/67e8/f887f680594e928dba4e0de9529203a2?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJK6APQGEHTP6I3PA%2F20180312%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20180312T042142Z&X-Amz-Expires=60&X-Amz-SignedHeaders=host&X-Amz-Signature=773937e746d3fb2f35b9bd55298317d9facbd55d78f87cb774cda36dd51628e5"
      title="call-to-action.complete-profile"
      subtitle="call-to-action.complete-profile-message"
      dismissText="remind-me-later"
      callToActionText="complete"
    />
  </LoggedInWrapper>
);

export default ScreenHome;
