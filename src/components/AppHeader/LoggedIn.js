// IMPORT libs
import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
// IMPORT components
import AvatarNotificationToolbar from "components/UI/Toolbar/AvatarNotification";
import { ThemedTabs, ThemedTab } from "components/UI/Tab";
// import SearchToolbar from "components/UI/Toolbar/Search";
import { ThemedAppBar } from "components/UI/AppBar";
import MobileDrawer from "components/Drawer";
// import list
import mobileList from "components/Drawer/Initialization";
// Component: Stateful
class LoggedInHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMobileDrawer: false
    };
  }
  render() {
    const { pathname, tabs, userProfile, isLoading, appComponent } = this.props;

    const { showMobileDrawer } = this.state;
    const selectedTab = this.getSelectedTabIndex(pathname, tabs);

    return (
      <ThemedAppBar color="default">
        {!isLoading && (
          <MobileDrawer
            showMobileDrawer={showMobileDrawer}
            toggleDrawer={this.toggleDrawer}
            appComponent={appComponent}
            userProfile={userProfile}
            list={mobileList}
          />
        )}
        {isLoading && <LinearProgress color="secondary" />}
        {
          //   tabs[selectedTab].value === "search" ? (
          //   <SearchToolbar
          //     isLoading={isLoading}
          //     thumbnail={userProfile.thumbnail}
          //     placeholder={tabs[selectedTab].name}
          //     toggleDrawer={this.toggleDrawer}
          //   />
          // ) : (
          <AvatarNotificationToolbar
            isLoading={isLoading}
            thumbnail={userProfile.thumbnail}
            title={tabs[selectedTab].name}
            toggleDrawer={this.toggleDrawer}
          />
          //  )
        }
        <ThemedTabs
          value={selectedTab}
          onChange={this.handleChange}
          indicatorColor="primary"
          scrollable
          fullWidth
        >
          {tabs.map(tab => <ThemedTab key={tab.name} icon={<tab.icon />} />)}
        </ThemedTabs>
      </ThemedAppBar>
    );
  }

  toggleDrawer = (side, open) => {
    this.setState({
      [side]: open
    });
  };

  handleChange = (event, value) => {
    const { history, tabs } = this.props;
    history.push(`/${tabs[value].value}`);
  };

  getSelectedTabIndex = (pathname, tabs) => {
    const selectedTab = tabs.filter((tab, index) =>
      pathname.toLowerCase().includes(tab.value)
    );
    return tabs.indexOf(selectedTab[0]);
  };
}
// PropTypes
LoggedInHeader.propTypes = {
  appComponent: PropTypes.object.isRequired,
  pathname: PropTypes.string.isRequired,
  tabs: PropTypes.array.isRequired
};
// Redux Store / State
const mapStateToProps = state => ({
  userProfile: state.user.profile,
  isLoading: state.app.is_loading
});

export default compose(connect(mapStateToProps), withRouter)(LoggedInHeader);
