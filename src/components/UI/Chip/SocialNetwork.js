// IMPORT libs
import React from "react";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Chip from "material-ui/Chip";
// IMPORT components
import { SocialNetworkAvatar } from "components/UI/Avatar";
// Custom Styles
const StyledChip = styled(Chip)`
  &&& {
    min-width: 230px;
    background: ${props => props.theme.social[props.color]};
    color: ${props => props.theme.text.icon};
    border-radius: ${props => props.theme.borderRadius.xs}px;
    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;
// Component: Stateless
const SocialNetworkChip = props => {
  const { buttonText, handleClick, color, intl } = props;
  return (
    <StyledChip
      color={color}
      onClick={handleClick}
      avatar={
        <SocialNetworkAvatar color={color}>
          <props.icon />
        </SocialNetworkAvatar>
      }
      label={intl.formatMessage({ id: buttonText })}
    />
  );
};
// PropTypes
SocialNetworkChip.propTypes = {
  buttonText: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired
};

export default injectIntl(SocialNetworkChip);
