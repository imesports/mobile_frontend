// IMPORT libs
import React from "react";
import { withRouter } from "react-router";
import styled from "styled-components";
import { connect } from "react-redux";
import ReactSVG from "react-svg";
import { compose } from "redux";
// IMPORT material
import Toolbar from "material-ui/Toolbar";
// IMPORT icons
import LightbulbOutlineIcon from "mdi-react/LightbulbOutlineIcon";
import LightbulbOnIcon from "mdi-react/LightbulbOnIcon";
import ArrowLeftIcon from "mdi-react/ArrowLeftIcon";
// IMPORT redux actions
import { swapTheme } from "reducers/app/actions";
// IMPORT components
import { ThemedIconButton } from "components/UI/Button";
import { ThemedAppBar } from "components/UI/AppBar";
import { LogoWrapper } from "components/UI/Wrapper";
import { LogoImg } from "components/UI/Img";
// IMPORT images
import Logo from "imgs/logo.svg";

// Custom Styles
const ThemedToolbar = styled(Toolbar)`
  &&& {
    padding: 0px ${props => props.theme.spacing.xs}px;
    display: flex;
    justify-content: space-between;
  }
`;

const IconDiv = styled.div`
   {
    min-width: ${props => props.theme.iconSize.default}px;
  }
`;

// Component: Stateless
const PublicHeader = props => (
  <ThemedAppBar position="absolute">
    <ThemedToolbar>
      <IconDiv>
        {props.backButton && (
          <ThemedIconButton onClick={props.history.goBack}>
            <ArrowLeftIcon />
          </ThemedIconButton>
        )}
      </IconDiv>

      <LogoWrapper backButton={props.backButton}>
        <LogoImg color="primary">
          <ReactSVG path={Logo} />
        </LogoImg>
      </LogoWrapper>

      <IconDiv
        onClick={() => {
          props.swapTheme();
          props.appComponent.forceUpdate();
        }}
      >
        <ThemedIconButton>
          {props.theme_color === "light" ? (
            <LightbulbOnIcon />
          ) : (
            <LightbulbOutlineIcon />
          )}
        </ThemedIconButton>
      </IconDiv>
    </ThemedToolbar>
  </ThemedAppBar>
);

// Access Redux Store
const mapStateToProps = state => ({
  theme_color: state.app.theme
});

export default compose(connect(mapStateToProps, { swapTheme }), withRouter)(
  PublicHeader
);
