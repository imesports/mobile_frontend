//IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
// IMPORT constants
import CONSTANTS from "appConstants";
// IMPORT material
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
import Card from "material-ui/Card";
// IMPORT components
import { NoButtonIcon } from "components/UI/IconFormatter";
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import CardContent from "components/UI/Card/Content";
// IMPORT utils
import getGameIcon from "util/helpers/getGameIcon";
// IMPORT styles
import CardWrapper from "components/UI/Wrapper/Card";
import {
  MyTeamsCardNameAndMembers,
  MyTeamsCardMemberSince,
  MyTeamsCardSecondRow,
  MyTeamsCardFirstRow,
  MyTeamsCardRoleName,
  MyTeamsCardRole
} from "components/Team/Wrapper";
// IMPORT icons
import TieIcon from "mdi-react/TieIcon";
import HeadsetIcon from "mdi-react/HeadsetIcon";
//Stateless Component
const MyTeamsCard = props => {
  const {
    avatar,
    teamId,
    teamName,
    teamMembers,
    gameName,
    userId,
    ownerId,
    memberSince,
    history
  } = props;

  const getPlayerRole = (userId, ownerId) => {
    if (userId === ownerId) {
      return (
        <MyTeamsCardRole>
          <NoButtonIcon fill="default" size="sm">
            <TieIcon />
          </NoButtonIcon>
          <MyTeamsCardRoleName>
            <Typography>
              <FormattedMessage id="owner" />
            </Typography>
          </MyTeamsCardRoleName>
        </MyTeamsCardRole>
      );
    }
    return (
      <MyTeamsCardRole>
        <NoButtonIcon fill="default" size="sm">
          <HeadsetIcon />
        </NoButtonIcon>
        <MyTeamsCardRoleName>
          <Typography>
            <FormattedMessage id="member" />
          </Typography>
        </MyTeamsCardRoleName>
      </MyTeamsCardRole>
    );
  };

  return (
    <CardWrapper>
      <Card onClick={() => history.push(`/team/${teamId}`)}>
        <CardContent>
          <MyTeamsCardFirstRow>
            <CustomSizeCircleAvatar
              src={avatar || CONSTANTS.DEFAULT_AVATAR_IMAGE}
              size="xl"
            />
            <MyTeamsCardNameAndMembers>
              <Typography variant="headline" noWrap>
                {teamName}
              </Typography>
              <Typography
                variant="body1"
                style={{ textTransform: "lowercase" }}
                noWrap
              >
                {teamMembers}{" "}
                {teamMembers === 1 ? (
                  <FormattedMessage id="member" />
                ) : (
                  <FormattedMessage id="members" />
                )}
              </Typography>
            </MyTeamsCardNameAndMembers>
            <NoButtonIcon size="default" fill="default">
              {getGameIcon(gameName)}
            </NoButtonIcon>
          </MyTeamsCardFirstRow>
        </CardContent>
        <Divider />
        <CardContent>
          <MyTeamsCardSecondRow>
            {getPlayerRole(userId, ownerId)}
            <MyTeamsCardMemberSince>
              <Typography>
                <FormattedMessage id="member-since" /> {memberSince}
              </Typography>
            </MyTeamsCardMemberSince>
          </MyTeamsCardSecondRow>
        </CardContent>
      </Card>
    </CardWrapper>
  );
};
// PropTypes
MyTeamsCard.propTypes = {
  teamMembers: PropTypes.number.isRequired,
  teamName: PropTypes.string.isRequired,
  gameName: PropTypes.string.isRequired,
  ownerId: PropTypes.string.isRequired,
  teamId: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  memberSince: PropTypes.string,
  avatar: PropTypes.string
};

export default withRouter(MyTeamsCard);
