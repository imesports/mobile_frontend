// IMPORT libs
import React from "react";
import { withRouter } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Button from "material-ui/Button";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";
// Sytled components
const StyledDialogActions = styled(DialogActions)`
  && {
    display: flex;
    justify-content: space-between;
  }
`;
// Component: Stateless
const DialogConfirmExitOrSave = ({
  description,
  showDialog,
  saveAction,
  removeDialog,
  goToOnClose,
  saveTitle,
  history
}) => (
  <div>
    <Dialog open={showDialog} onClose={removeDialog}>
      <DialogTitle>
        <FormattedMessage id="leaving-without-save" />
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <FormattedMessage id={description} />
        </DialogContentText>
      </DialogContent>
      <StyledDialogActions>
        <Button onClick={() => history.push(goToOnClose)} color="primary">
          <FormattedMessage id="leave" />
        </Button>
        <Button onClick={saveAction} color="primary" autoFocus>
          <FormattedMessage id={saveTitle} />
        </Button>
      </StyledDialogActions>
    </Dialog>
  </div>
);
// PropTypes
DialogConfirmExitOrSave.propTypes = {
  showDialog: PropTypes.bool.isRequired,
  saveAction: PropTypes.func.isRequired,
  description: PropTypes.string.isRequired,
  removeDialog: PropTypes.func.isRequired,
  goToOnClose: PropTypes.string.isRequired,
  saveTitle: PropTypes.string.isRequired
};

export default withRouter(DialogConfirmExitOrSave);
