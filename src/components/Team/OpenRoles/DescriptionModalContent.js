// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import { DialogActions, DialogContent } from "material-ui/Dialog";
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
import Button from "material-ui/Button";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { LogoWrapper, FormInputSpacing } from "components/UI/Wrapper";
import { ThickDivider } from "components/UI/Divider";
import { LogoImg } from "components/UI/Img";
// IMPORT images
import PubgLogo from "imgs/delete_later/pubg.png";
// IMPORT from same folder
import {
  RoleWrapper,
  DescriptionWrapper,
  RequisitesItem,
  RequisitesTitle
} from "./Wrapper";
// Custom Styles
const DialogContentWrapper = styled.div`
  @media screen and (min-width: 370px) {
    min-width: 300px;
  }
  @media screen and (min-width: 600px) {
    min-width: 500px;
  }
`;

const GameWrapper = styled(LogoWrapper)`
  && {
    padding: ${props => props.theme.spacing.default}px;
    background-color: ${props => props.theme.background.innerCard};
  }
`;

const RequisitesWrapper = styled.div`
  margin-top: ${props => props.theme.spacing.default}px;
`;
// Component: Stateless
const OpenRoleDescriptionModal = ({
  avatar,
  role,
  data,
  teamInfo,
  handleClose
}) => (
  <DialogContentWrapper>
    <GameWrapper>
      <LogoImg src={PubgLogo} alt="PUBG" />
    </GameWrapper>
    <ThickDivider />
    <DialogContent>
      <FormInputSpacing />
      <RoleWrapper flexStart={true}>
        <CustomSizeCircleAvatar size="xxl" src={avatar} />
        <DescriptionWrapper>
          <RequisitesTitle variant="subheading">
            <FormattedMessage id="search.role" />:{" "}
            <FormattedMessage id={role} />
          </RequisitesTitle>
          <Typography>
            <FormattedMessage id="team" />: {teamInfo.teamName}
          </Typography>
        </DescriptionWrapper>
      </RoleWrapper>
      <RequisitesWrapper>
        <RequisitesTitle variant="subheading">
          <FormattedMessage id="search.minimum-requisites" />
        </RequisitesTitle>
        <FormInputSpacing />
        {data.map((item, index) => (
          <RequisitesItem key={item.label}>
            <FormattedMessage id={item.label} />:{" "}
            {Array.isArray(item.description)
              ? item.description.join(", ")
              : item.description}
          </RequisitesItem>
        ))}
      </RequisitesWrapper>
    </DialogContent>
    <Divider />
    <DialogActions>
      <Button onClick={handleClose} color="primary">
        <FormattedMessage id="button.cancel" />
      </Button>
      <Button onClick={handleClose} color="primary" autoFocus>
        <FormattedMessage id="button.apply" />
      </Button>
    </DialogActions>
  </DialogContentWrapper>
);

export default OpenRoleDescriptionModal;
