import styled from "styled-components";

export const DrawerContentWrapper = styled.div`
  && {
    width: 300px;

    @media screen and (max-width: 325px) {
      width: 250px;
    }

    @media screen and (max-width: 275px) {
      width: 200px;
    }
  }
`;
