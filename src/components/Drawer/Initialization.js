// import MessageAlertIcon from "mdi-react/MessageAlertIcon";
import GamesIcon from "material-ui-icons/VideogameAsset";
import LightbulbIcon from "mdi-react/LightbulbIcon";
// import HelpCircleIcon from "mdi-react/HelpCircleIcon";
import AccountBoxIcon from "mdi-react/AccountBoxIcon";
import ExitToAppIcon from "mdi-react/ExitToAppIcon";
// import NewspaperIcon from "mdi-react/NewspaperIcon";
// import SettingsIcon from "mdi-react/SettingsIcon";
// import LadybugIcon from "mdi-react/LadybugIcon";
import TeamIcon from "material-ui-icons/Group";
import HomeIcon from "mdi-react/HomeIcon";

export default [
  {
    id: "profile",
    listCategoryContent: [
      {
        link: "/home",
        icon: HomeIcon,
        name: "home"
      },
      {
        link: "/player",
        icon: AccountBoxIcon,
        name: "my-profile"
      },
      {
        link: "/my-teams",
        icon: TeamIcon,
        name: "my-teams"
      },
      {
        link: "/my-games",
        icon: GamesIcon,
        name: "my-games"
      }
      // {
      //   link: "/news",
      //   icon: NewspaperIcon,
      //   name: "news"
      // }
    ]
  },
  {
    id: "logout",
    listCategoryName: "",
    listCategoryContent: [
      {
        link: "function",
        icon: ExitToAppIcon,
        name: "logout"
      }
    ]
  },
  {
    id: "settings",
    listCategoryName: "",
    listCategoryContent: [
      {
        link: "function",
        icon: LightbulbIcon,
        name: "swap-theme"
      }
      // {
      //   link: "/settings",
      //   icon: SettingsIcon,
      //   name: "settings"
      // },
      // {
      //   link: "/help",
      //   icon: HelpCircleIcon,
      //   name: "help"
      // },
      // {
      //   link: "/bugreport",
      //   icon: LadybugIcon,
      //   name: "report-bug"
      // },
      // {
      //   link: "/feedback",
      //   icon: MessageAlertIcon,
      //   name: "send-feedback"
      // }
    ]
  }
];
