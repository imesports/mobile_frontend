// IMPORT libs
import React from "react";
// IMPORT components
import { PublicWrapper } from "components/UI/Wrapper";
import styled from "styled-components";
// Custom Styles
const DivMargin = styled.div`
   {
    padding: 30px;
  }
`;
// Screen: Stateless
const ScreenNotFound = () => (
  <PublicWrapper>
    <DivMargin>
      <span>Page Not Found</span>
    </DivMargin>
  </PublicWrapper>
);

export default ScreenNotFound;
