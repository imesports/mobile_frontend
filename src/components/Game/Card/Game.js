// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
import Card, { CardMedia } from "material-ui/Card";
// import Typography from "material-ui/Typography";
// IMPORT icons
import DotsVerticalIcon from "mdi-react/DotsVerticalIcon";
// IMPORT components
import { WhiteTypography } from "components/UI/Typography";
import { DefaultIcon } from "components/UI/IconFormatter";
import CardWrapper from "components/UI/Wrapper/Card";
// import CardContent from "components/UI/Card/Content";
// import { AccentButton } from "components/UI/Button";
import MenuGameOptions from "components/Game/Menu/GameOptions";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
  height: 200px;
  position: relative;
`;

const ImageTitleWrapper = styled.div`
   {
    position: absolute;
    bottom: 0;
    padding: ${props => props.theme.spacing.default}px;
    width: 100%;
  }
`;

// const StyledCardContend = styled(CardContent)`
//   &&& {
//     display: flex;
//     justify-content: space-between;
//     align-items: center;
//   }
// `;

const ImageIconWrapper = styled.div`
   {
    fill: ${props => props.theme.text.white};
    position: absolute;
    top: ${props => props.theme.spacing.xs}px;
    right: ${props => props.theme.spacing.xs}px;
  }
`;

const DarkenBackgroud = styled.div`
   {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: ${props => props.theme.background.black}77;
  }
`;
// Component: Stateless
class GameCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gameOptions: null,
      loading: false
    };
  }
  render() {
    const {
      gameId,
      cardImage,
      title,
      // subtitle,
      // subtitleValue,
      // statsName,
      // stats,
      // statsDiff,
      // buttonText,
      // buttonGoTo,
      // history,
      intl
    } = this.props;

    const { gameOptions, loading } = this.state;

    return (
      <CardWrapper>
        {loading && <LinearProgress color="secondary" />}
        <Card>
          <StyledCardMedia
            image={cardImage}
            title={intl.formatMessage({ id: title })}
          >
            <DarkenBackgroud />
            <ImageTitleWrapper>
              <WhiteTypography variant="headline" noWrap>
                <FormattedMessage id={title} />
              </WhiteTypography>
              {/*<WhiteTypography variant="body1">
                <FormattedMessage id={subtitle} />: <b>{subtitleValue}</b>
              </WhiteTypography>*/}
            </ImageTitleWrapper>
            <ImageIconWrapper>
              <DefaultIcon size="sm" fill="white">
                <DotsVerticalIcon onClick={this.openMenu("gameOptions")} />
              </DefaultIcon>
            </ImageIconWrapper>
          </StyledCardMedia>
          {/*<StyledCardContend>
            <Typography variant="button">
              <b>{statsName && <FormattedMessage id={statsName} />}</b>
            </Typography>
            <Typography variant="button">
              {stats && statsDiff && `${stats} (${statsDiff})`}
            </Typography>
            <AccentButton onClick={() => history.push(buttonGoTo)}>
              <b>
                <FormattedMessage id={buttonText} />
              </b>
            </AccentButton>
          </StyledCardContend>*/}
        </Card>
        <MenuGameOptions
          anchorEl={gameOptions}
          closeAction={() => this.closeMenu("gameOptions")}
          gameId={gameId}
          loadingToggle={this.loadingToggle}
          gameAbbreviation={`${title}.abbreviation`}
        />
      </CardWrapper>
    );
  }

  openMenu = menu => event => {
    this.setState({
      [menu]: event.currentTarget
    });
  };

  closeMenu = menu => {
    this.setState({
      [menu]: null
    });
  };

  loadingToggle = value => {
    this.setState({
      loading: value
    });
  };
}

// PropTypes
GameCard.propTypes = {
  gameId: PropTypes.number.isRequired,
  cardImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  subtitleValue: PropTypes.string.isRequired,
  statsName: PropTypes.string,
  stats: PropTypes.string,
  statsDiff: PropTypes.string,
  buttonText: PropTypes.string.isRequired,
  buttonGoTo: PropTypes.string.isRequired
};

export default compose(injectIntl, withRouter)(GameCard);
