/* IMPORT libs */
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
/* IMPORT constants */
import CONSTANTS from "appConstants";
/* IMPORT helpers */
import getGameIcon from "util/helpers/getGameIcon";
/* IMPORT material */
import Card, { CardMedia } from "material-ui/Card";
import Typography from "material-ui/Typography";
/* IMPORT components*/
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { NoButtonIcon } from "components/UI/IconFormatter";
import CardContent from "components/UI/Card/Content";
/* IMPORT styles */
import CardWrapper from "components/UI/Wrapper/Card";

const StyledCardMedia = styled(CardMedia)`
   {
    position: relative;
    padding-bottom: calc(100% / 3);
    width: 100%;
    margin-bottom: calc(${props => props.theme.iconSize.xxl}px / 2);
  }
  @media screen and (min-width: 600px) {
    padding-bottom: ${props => props.theme.cardMediaSize.xl}px;
  }
`;

const AvatarPosition = styled.div`
   {
    position: absolute;
    bottom: calc(-${props => props.theme.iconSize.xxl}px / 2);
    left: ${props => props.theme.spacing.default}px;
  }
`;

const GameAndMembers = styled.div`
   {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

const GameIconAndName = styled.div`
   {
    display: flex;
    align-items: center;

    *:first-child {
      margin-right: ${props => props.theme.spacing.xs}px;
    }
  }
`;
/* Stateles component */
const ProfileBasicInformation = props => {
  const { avatar, background_image, name, game, positions } = props;

  return (
    <CardWrapper>
      <Card>
        <StyledCardMedia
          image={background_image || CONSTANTS.DEFAULT_BACKGROUND_IMAGE}
        >
          <AvatarPosition>
            <CustomSizeCircleAvatar
              src={avatar || CONSTANTS.DEFAULT_AVATAR_IMAGE}
              size="xxl"
            />
          </AvatarPosition>
        </StyledCardMedia>
        <CardContent>
          <Typography variant="headline" noWrap>
            {name}
          </Typography>
          <GameAndMembers>
            <GameIconAndName>
              <NoButtonIcon fill="default" size="sm">
                {getGameIcon(game.name)}
              </NoButtonIcon>
              <Typography variant="caption">
                <FormattedMessage id={`${game.name}.abbreviation`} />
              </Typography>
            </GameIconAndName>
            <Typography
              variant="caption"
              style={{ textTransform: "lowercase" }}
            >
              {positions.length}{" "}
              <FormattedMessage
                id={positions.length > 1 ? "members" : "member"}
              />
            </Typography>
          </GameAndMembers>
        </CardContent>
      </Card>
    </CardWrapper>
  );
};

ProfileBasicInformation.propTypes = {
  positions: PropTypes.array.isRequired,
  background_image: PropTypes.string,
  game: PropTypes.object.isRequired,
  avatar: PropTypes.string,
  name: PropTypes.string
};

export default ProfileBasicInformation;
