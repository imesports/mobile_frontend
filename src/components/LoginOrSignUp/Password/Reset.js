// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import { Link } from "react-router-dom";
// IMPORT material
import Card from "material-ui/Card";
import Typography from "material-ui/Typography";
// IMPORT components
import PasswordWithToggleView from "components/UI/Input/PasswordWithToggleView";
import { FormInputSpacing, FormOptionsWrapper } from "components/UI/Wrapper";
import CardHeaderTitleSubtitle from "components/UI/Card/Header/TitleSubtitle";
import { RaisedAccentButton } from "components/UI/Button";
import CardContent from "components/UI/Card/Content";
// IMPORT from parent folder
import { LoginFormWrapper, ButtonWrapper } from "../Wrapper";
// Custom Styles
const StyledCard = styled(Card)`
  width: 100%;
  max-width: 450px;

  @media screen and (min-width: 450px) {
    margin-top: ${props => props.theme.spacing.xl}px;
  }
`;

// Component: Stateful
class LoginPasswordReset extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: "",
      success: false
    };
  }

  render() {
    const { intl } = this.props;

    return (
      <LoginFormWrapper>
        <StyledCard>
          <CardContent>
            {this.state.success ? (
              <CardHeaderTitleSubtitle
                title="reset-password.success-title"
                subtitle="reset-password.success-subtitle"
              />
            ) : (
              <div>
                <CardHeaderTitleSubtitle
                  title="reset-password"
                  subtitle="reset-password.subtitle"
                />

                <FormInputSpacing>
                  <PasswordWithToggleView
                    label="reset-password.label"
                    placeholder={intl.formatMessage({
                      id: "reset-password.placeholder"
                    })}
                    password={this.state.password}
                    handleChange={this.handleChange}
                  />
                </FormInputSpacing>

                <ButtonWrapper>
                  <RaisedAccentButton onClick={this.handleClick}>
                    <FormattedMessage id="reset-password" />
                  </RaisedAccentButton>
                </ButtonWrapper>
              </div>
            )}
          </CardContent>
          <FormOptionsWrapper>
            <Link to="/login">
              <Typography variant="caption">
                <FormattedMessage id="forgot-password.try-login" />
              </Typography>
            </Link>
          </FormOptionsWrapper>
        </StyledCard>
      </LoginFormWrapper>
    );
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleClick = () => {
    this.setState(prevState => ({
      success: !prevState.success
    }));
  };
}

export default injectIntl(LoginPasswordReset);
