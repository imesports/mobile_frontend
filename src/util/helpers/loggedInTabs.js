// IMPORT icons
import GamesIcon from "material-ui-icons/VideogameAsset";
// import ChampionshipIcon from "mdi-react/TrophyIcon";
// import SearchIcon from "material-ui-icons/Search";
import TeamIcon from "material-ui-icons/Group";
import HomeIcon from "material-ui-icons/Home";

export default [
  {
    name: "logged-in.home-page-name",
    icon: HomeIcon,
    value: "home"
  },
  {
    name: "logged-in.team-page-name",
    icon: TeamIcon,
    value: "my-teams"
  },
  {
    name: "logged-in.games-page-name",
    icon: GamesIcon,
    value: "my-games"
  }
  // {
  //   name: "logged-in.championship-page-name",
  //   icon: ChampionshipIcon,
  //   value: "championships"
  // },
  // {
  //   name: "logged-in.search-placeholder",
  //   icon: SearchIcon,
  //   value: "search"
  // }
];
