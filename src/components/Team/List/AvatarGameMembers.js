import React from "react";
import PropTypes from "prop-types";

import TeamCardAvatarGameMembers from "components/Team/Card/AvatarGameMembers";
import CircularProgress from "components/UI/Progress/Circular";

const MyTeamsList = props => {
  const { teamListLoading, teamsList, userId } = props;

  if (teamListLoading) return <CircularProgress />;

  if (teamsList && teamsList.length === 0) {
    return <div>No Teams yet</div>; // TODO: Create "No Teams yet" component
  }

  return (
    <div>
      {teamsList.length > 0 &&
        teamsList.map(content => (
          <TeamCardAvatarGameMembers
            key={`MyTeamsList ${content.id}`}
            avatar={content.avatar}
            teamName={content.name}
            teamMembers={numberOfMembers(content.owner_id, content.positions)}
            teamId={content.id}
            gameName={content.game.name}
            userId={userId}
            ownerId={content.owner_id}
            memberSince="MONTH/YEAR"
          />
        ))}
    </div>
  );
};

const numberOfMembers = (ownerId, teamPositions) => {
  // Count number of players actives in the team
  let members = teamPositions.filter(_ => _.is_active === true).length;

  if (teamPositions.findIndex(_ => _.user_id === ownerId) === -1) {
    // If the owner doesnt occupy a vacancy, sum 1
    members++;
  }

  return members;
};

// PropTypes
MyTeamsList.propTypes = {
  teamListLoading: PropTypes.bool,
  teamsList: PropTypes.array,
  userId: PropTypes.string
};

export default MyTeamsList;
