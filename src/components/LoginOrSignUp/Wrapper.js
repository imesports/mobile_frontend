import styled from "styled-components";

export const LoginFormWrapper = styled.div`
   {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const ButtonWrapper = styled.div`
  margin: ${props => props.theme.spacing.default}px 0;
  display: flex;
  flex-direction: row-reverse;
  justify-content: space-between;
  align-items: flex-start;
`;

export const SocialNetWrapper = styled.div`
  margin-top: ${props => props.theme.spacing.xl}px;
  display: flex;
  flex-direction: column;
  justify-contet: space-around;
  align-items: center;
`;
