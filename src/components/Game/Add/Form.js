// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from "react-intl";
import PropTypes from "prop-types";
// IMPORT fetch
import { gameListFetch } from "util/game";
// IMPORT material
import { CircularProgress } from "material-ui/Progress";
import MenuItem from "material-ui/Menu/MenuItem";
import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { NoButtonIcon } from "components/UI/IconFormatter";
import { FormInputSpacing } from "components/UI/Wrapper";
// IMPORT Same Folder
import {
  IntegrationRecommendationWrapper,
  NoGameSelectedMessageWrapper,
  SelectedGameHeaderWrapper,
  GameDescriptionWrapper,
  GameNameWrapper
} from "./Wrapper";
// IMPORT Icons
import ArrowUpBoldHexagonOutlineIcon from "mdi-react/ArrowUpBoldHexagonOutlineIcon";

// Component: Stateful
class GameAddForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      localLoading: true,
      gameList: null,
      gameInfo: null
    };
  }

  componentDidMount() {
    if (this.props.token && this.props.userId && !this.state.gameList) {
      this.getGamesList(this.props.token);
    }
  }

  render() {
    const {
      intl,
      selectedGame,
      selectGame,
      playerGames,
      loading = false
    } = this.props;
    const { gameList, gameInfo, localLoading } = this.state;

    return (
      <div>
        {localLoading && (
          <div style={{ textAlign: "center" }}>
            <CircularProgress />
          </div>
        )}
        {gameList &&
          playerGames && (
            <div>
              <TextField
                disabled={loading}
                id="create_team-team_language"
                select
                label={intl.formatMessage({ id: "select-game" })}
                fullWidth
                value={selectedGame}
                onChange={e => {
                  this.defineGameInfo(e.target.value, gameList);
                  selectGame(e.target.value); // Came from props
                }}
              >
                {gameList
                  .sort(_ => this.verifyUserHasGame(_.id, playerGames))
                  .map(content => (
                    <MenuItem
                      key={`${content.name}${content.id}`}
                      value={content.id}
                      disabled={this.verifyUserHasGame(content.id, playerGames)}
                    >
                      <FormattedMessage id={content.name} />
                    </MenuItem>
                  ))}
              </TextField>
              <FormInputSpacing />
              {gameInfo ? (
                <div>
                  <SelectedGameHeaderWrapper>
                    <CustomSizeCircleAvatar src={gameInfo.avatar} size="xxl" />
                    <GameNameWrapper>
                      <Typography variant="display2">
                        <FormattedMessage
                          id={`${gameInfo.name}.abbreviation`}
                        />
                      </Typography>
                      <Typography variant="caption">
                        <FormattedMessage id={gameInfo.name} />
                      </Typography>
                    </GameNameWrapper>
                  </SelectedGameHeaderWrapper>
                  <GameDescriptionWrapper>
                    <Typography>
                      <FormattedMessage id={`${gameInfo.name}.description`} />
                    </Typography>
                  </GameDescriptionWrapper>
                  <IntegrationRecommendationWrapper>
                    <Typography variant="caption">
                      <FormattedMessage id="integration-recommendation" />
                    </Typography>
                  </IntegrationRecommendationWrapper>
                </div>
              ) : (
                <NoGameSelectedMessageWrapper>
                  <NoButtonIcon fill="default" size="xxxl">
                    <ArrowUpBoldHexagonOutlineIcon />
                  </NoButtonIcon>
                  <Typography variant="caption" align="center">
                    <FormattedHTMLMessage id="please-select-a-game" />
                  </Typography>
                </NoGameSelectedMessageWrapper>
              )}
            </div>
          )}
      </div>
    );
  }

  getGamesList = token => {
    this.setState({ localLoading: true });
    gameListFetch(token).then(res => {
      if (res.ok) {
        res.json().then(gameList => {
          this.setState({ gameList: gameList });
          this.setState({ localLoading: false });
        });
      } else {
        // Treatment of Error
      }
    });
  };

  defineGameInfo = (value, gameList) => {
    const selectGame = gameList.filter(_ => _.id === value);

    this.setState({
      gameInfo: selectGame[0]
    });
  };

  verifyUserHasGame = (gameId, playerGames) => {
    return playerGames.findIndex(i => i.id === gameId) === -1 ? false : true;
  };
}
// PropTypes
GameAddForm.propTypes = {
  selectedGame: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  playerGames: PropTypes.array.isRequired,
  selectGame: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  token: PropTypes.string.isRequired
};

export default injectIntl(GameAddForm);
