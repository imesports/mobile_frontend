// IMPORT libs
import styled from "styled-components";
import Typography from "material-ui/Typography";

// Custom Styles
export const WhiteTypography = styled(Typography)`
  && {
    color: ${props => props.theme.text.white};
  }
`;

export const StyledTypography = styled(Typography)`
  && {
    color: ${props => props.theme.text.white};
  }
`;

export const ThemedTypography = styled(Typography)`
  && {
    flex: 1;
  }
`;
