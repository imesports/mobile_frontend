// IMPORT libs
import React from "react";
// IMPORT icons
// import PlusCircleOutlineIcon from "mdi-react/PlusCircleOutlineIcon";
import GamepadVariantIcon from "mdi-react/GamepadVariantIcon";
// IMPORT components
import PlusFABWithList from "components/UI/Button/PlusFABWithList";
import WithFABWrapper from "components/UI/Wrapper/WithFABWrapper";
import { LoggedInWrapper } from "components/UI/Wrapper";
import MyGames from "components/Game/MyGames";

const list = [
  {
    title: "add-game",
    goTo: "/my-games/add",
    icon: <GamepadVariantIcon />
  }
  // {
  //   title: "add-integration",
  //   goTo: "/integrations",
  //   icon: <PlusCircleOutlineIcon />
  // }
];

// Screen: Stateless
const ScreenMyGames = props => (
  <LoggedInWrapper>
    <WithFABWrapper>
      <MyGames />
      <PlusFABWithList list={list} variant="fab" />
    </WithFABWrapper>
  </LoggedInWrapper>
);

export default ScreenMyGames;
