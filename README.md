# I'm eSports

This is I'm eSports frontend, an React.js application built from scratch.

## Getting Started / How to run

You need Node.js to run project. We recommend the most recent version.

After fetching this repository, install all dependencies with:

    npm install

And start the webapp using:

    npm start

Point your browser to http://localhost:3000

### Fetch latest i18n strings from Google Drive spreadsheet automatically

    npm run update-i18n

Spreadsheet can be found inside company drive -> dev -> IM eSports i18n (spreadsheet)

### For committing in the best way, please follow the following guidelines:

    https://udacity.github.io/git-styleguide/


TODO: write a decent readme
