// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import { withRouter } from "react-router";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT components
import { StyledTypography } from "components/UI/Typography";
// IMPORT from same folder
import { SignUpButton } from "./Button";
// Custom Styles
const ReleaseDateWrapper = styled.div`
   {
    background: ${props => props.theme.background.purple};
    padding: ${props => props.theme.spacing.xl}px;
    text-align: center;
  }
`;
// Component: Stateless
const LandingPageReleaseDate = ({
  textFirstPart,
  textSecondPart,
  buttonText,
  history
}) => (
  <ReleaseDateWrapper>
    <StyledTypography variant="body1">
      <FormattedMessage id={textFirstPart} />
    </StyledTypography>
    <StyledTypography variant="body2">
      <FormattedHTMLMessage id={textSecondPart} />
    </StyledTypography>
    <SignUpButton onClick={() => history.push("/signup")}>
      <FormattedHTMLMessage id={buttonText} />
    </SignUpButton>
  </ReleaseDateWrapper>
);

// PropTypes
LandingPageReleaseDate.propTypes = {
  textFirstPart: PropTypes.string.isRequired,
  textSecondPart: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired
};

export default withRouter(LandingPageReleaseDate);
