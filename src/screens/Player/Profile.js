// IMPORT libs
import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { compose } from "redux";
// IMPORT util
import { profileFetch } from "util/user";
// IMPORT components
import PlayerPersonalInfoCard from "components/Player/Profile/PersonalInfo/Card";
import CircularProgress from "components/UI/Progress/Circular";
import { LoggedInWrapper } from "components/UI/Wrapper";
// Screen: Stateful
class ScreenPlayerProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playerInfo: null,
      loading: false,
      error: false
    };
  }

  componentDidMount() {
    const { token } = this.props;
    const { player_id } = this.props.match.params;
    this.getProfile(token, player_id);
  }

  componentWillReceiveProps(nextProps) {
    const { player_id } = nextProps.match.params;
    if (this.props.match.params.player_id !== player_id) {
      const { token } = this.props;
      this.getProfile(token, player_id);
    }
  }

  render() {
    const { playerInfo, error, loading } = this.state;
    const ownPlayerInfo = { ...playerInfo, ownerProfile: true };
    if (loading) {
      return (
        <LoggedInWrapper toolbar={true}>
          <CircularProgress />
        </LoggedInWrapper>
      );
    }
    return (
      <div>
        {playerInfo && (
          <LoggedInWrapper toolbar={true}>
            <PlayerPersonalInfoCard userData={ownPlayerInfo} />
          </LoggedInWrapper>
        )}
        {error && (
          <LoggedInWrapper toolbar={true}>Algo deu errado.</LoggedInWrapper>
        )}
      </div>
    );
  }

  getProfile = (token, player_id) => {
    this.setState({ error: false });
    this.setState({ loading: true });
    profileFetch(token, player_id).then(res => {
      this.setState({ loading: false });
      if (res.ok) {
        res.json().then(player => {
          // Treatment of Error (user didn't go through the SignUp Last Step)
          if (player.first_name === "") {
            this.setState({ error: true });
          } else {
            this.setState({ playerInfo: player });
          }
        });
      } else {
        // Treatment of Error (user don't exist)
        this.setState({ error: true });
      }
    });
  };
}

const mapStateToProps = state => ({
  token: state.user.token
});

export default compose(connect(mapStateToProps), withRouter)(
  ScreenPlayerProfile
);
