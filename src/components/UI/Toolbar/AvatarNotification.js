// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT icons
// import BellIcon from "mdi-react/BellIcon";
// IMPORT components
import { ThemedTypography } from "components/UI/Typography";
import ToolbarAvatar from "components/UI/Avatar/Toolbar";
import { ThemedIconButton } from "components/UI/Button";
// IMPORT from same folder
import StyledToolbar from "./StyledToolbar";
// Component: Stateless
const AvatarNotificationToolbar = ({
  title,
  toggleDrawer,
  thumbnail,
  isLoading
}) => (
  <StyledToolbar>
    <ToolbarAvatar
      isLoading={isLoading}
      onClick={() => toggleDrawer("showMobileDrawer", true)}
      thumbnail={thumbnail}
    />
    <ThemedTypography variant="title">
      <FormattedMessage id={title} />
    </ThemedTypography>
    <ThemedIconButton aria-label="View notifications">
      {/*<BellIcon />*/}
    </ThemedIconButton>
  </StyledToolbar>
);

// PropTypes
AvatarNotificationToolbar.propTypes = {
  title: PropTypes.string.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  thumbnail: PropTypes.string
};

export default AvatarNotificationToolbar;
