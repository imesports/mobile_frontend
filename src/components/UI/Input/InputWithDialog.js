// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Input, { InputLabel, InputAdornment } from "material-ui/Input";
import { FormControl } from "material-ui/Form";
// IMPORT icons
import InformationOutlineIcon from "mdi-react/InformationOutlineIcon";
// IMPORT components
import { NoButtonIcon } from "components/UI/IconFormatter";
import TooltipDialog from "components/UI/Dialog/Tooltip";
// Component: Stateful
class InputWithDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDialog: false
    };
  }
  render() {
    const {
      id,
      label,
      placeholder,
      value,
      disabled = false,
      handleChange,
      dialogTitle,
      dialogText,
      maxLength,
      dialogButtonText,
      intl
    } = this.props;

    return (
      <FormControl fullWidth>
        <InputLabel htmlFor={id}>
          <FormattedMessage id={label} />
        </InputLabel>
        <Input
          id={id}
          placeholder={intl.formatMessage({ id: placeholder })}
          value={value}
          onChange={handleChange}
          disabled={disabled}
          endAdornment={
            <InputAdornment position="end">
              <NoButtonIcon
                fill="primary"
                onClick={this.toggleDialog("showDialog", true)}
              >
                <InformationOutlineIcon />
              </NoButtonIcon>
            </InputAdornment>
          }
          inputProps={{ maxLength: maxLength }}
        />
        <TooltipDialog
          title={dialogTitle}
          text={dialogText}
          buttonText={dialogButtonText}
          showDialog={this.state.showDialog}
          toggleDialog={this.toggleDialog}
        />
      </FormControl>
    );
  }

  toggleDialog = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };
}
// PropTypes
InputWithDialog.propTypes = {
  id: PropTypes.string.isRequired,
  stateName: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  dialogTitle: PropTypes.string.isRequired,
  dialogText: PropTypes.string.isRequired,
  dialogButtonText: PropTypes.string.isRequired
};

export default injectIntl(InputWithDialog);
