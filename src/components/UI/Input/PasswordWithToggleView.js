// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Input, { InputLabel, InputAdornment } from "material-ui/Input";
import { FormControl } from "material-ui/Form";
// IMPORT icons
import EyeOffIcon from "mdi-react/EyeOffIcon";
import EyeIcon from "mdi-react/EyeIcon";
// IMPORT components
import { ThemedIconButton } from "components/UI/Button";
// Component: Stateful
class PasswordWithToggleView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false
    };
  }
  render() {
    const {
      label,
      placeholder,
      password,
      handleChange,
      disabled = false
    } = this.props;
    const { showPassword } = this.state;

    return (
      <FormControl fullWidth>
        <InputLabel htmlFor="password">
          <FormattedMessage id={label} />
        </InputLabel>
        <Input
          disabled={disabled}
          id="password"
          type={showPassword ? "text" : "password"}
          placeholder={placeholder}
          value={password}
          onChange={handleChange("password")}
          endAdornment={
            <InputAdornment position="end">
              <ThemedIconButton
                onClick={this.handleClickShowPasssword}
                onMouseDown={this.handleMouseDownPassword}
              >
                {showPassword ? <EyeOffIcon /> : <EyeIcon />}
              </ThemedIconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    );
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPasssword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
}
// PropTypes
PasswordWithToggleView.propTypes = {
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

export default PasswordWithToggleView;
