import CONSTANTS from "appConstants";

export const gameListFetch = token =>
  fetch(`${CONSTANTS.BACKEND_URL}/games/`, {
    method: "GET",
    headers: {
      Authorization: `JWT ${token}`
    }
  });

export const playerGamesFetch = (token, userId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-games/${userId}/`, {
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      Authorization: `JWT ${token}`
    }
  });

export const addGameFetch = (token, userId, gameId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-games/${userId}/add/${gameId}/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });

export const removeGameFetch = (token, userId, gameId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-games/${userId}/remove/${gameId}/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });
