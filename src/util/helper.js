import CONSTANTS from "appConstants";
export const validateLocale = locale => {
  switch (locale) {
    case "en":
      return "en-US";
    case "pt":
      return "pt-BR";
    case "es":
      return "es-ES";
    default:
      return CONSTANTS.DEFAULT_LOCALE; // Return locale when API supports global locales
  }
};
