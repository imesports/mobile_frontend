// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import styled from "styled-components";
import PropTypes from "prop-types";

// IMPORT from same folder
import { SignUpTitle, SignUpSubtitle } from "./Typography";
import { SignUpButton } from "./Button";

// Custom Styles
const SignUpWrapper = styled.div`
  {
    padding: 180px ${props => props.theme.spacing.default}px 80px;
    text-align: center;
    background-size: cover;

    background-position: center center;
    background-image: url(" ${props => props.backgroundImg} ");
  }
`;

const MiniBar = styled.div`
   {
    background-color: ${props => props.theme.primary.default};
    height: 4px;
    width: 86px;

    margin-left: auto;
    margin-right: auto;

    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;

// Component: Stateless
const LandingPageLoginOrSignUp = ({
  title,
  subtitle,
  signUp,
  logIn,
  backgroundImg,
  history
}) => (
  <SignUpWrapper backgroundImg={backgroundImg}>
    <SignUpSubtitle>
      <FormattedMessage id={title} />
    </SignUpSubtitle>
    <MiniBar />
    <SignUpTitle>
      <FormattedMessage id={subtitle} />
    </SignUpTitle>
    <SignUpButton onClick={() => history.push("/signup")}>
      <FormattedMessage id={signUp} />
    </SignUpButton>
    <SignUpButton onClick={() => history.push("/login")}>
      <FormattedMessage id={logIn} />
    </SignUpButton>
  </SignUpWrapper>
);

// PropTypes
LandingPageLoginOrSignUp.propTypes = {
  backgroundImg: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  signUp: PropTypes.string.isRequired,
  logIn: PropTypes.string.isRequired
};

export default withRouter(LandingPageLoginOrSignUp);
