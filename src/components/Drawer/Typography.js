import styled from "styled-components";

import Typography from "material-ui/Typography";

export const PrimaryTypography = styled(Typography)`
  && {
    color: ${props => props.theme.text.primary};
  }
`;
