// IMPORT libs
import React from "react";
import { withRouter } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT icons
import ArrowLeftIcon from "mdi-react/ArrowLeftIcon";
import PencilIcon from "mdi-react/PencilIcon";
import CloseIcon from "mdi-react/CloseIcon";
// IMPORT components
import ActionButton from "components/UI/Button/ActionButton";
import { ThemedTypography } from "components/UI/Typography";
import { DefaultIcon } from "components/UI/IconFormatter";
// IMPORT from same folder
import StyledToolbar from "./StyledToolbar";
// Custom Styles
const StyledActionButton = styled(ActionButton)`
  && {
    font-size: 1em;
    line-height: 0px;
  }
`;
// Component: Stateless
const CloseSaveToolbar = ({
  title,
  closeIconAction,
  formattedTitle,
  closeIcon,
  backIcon,
  saveButton,
  saveAction,
  editIcon,
  history,
  isLoading = false
}) => (
  <StyledToolbar>
    <DefaultIcon fill="default">
      {closeIcon && (
        <CloseIcon
          onClick={() => {
            if (closeIconAction) return closeIconAction();
            return history.goBack();
          }}
        />
      )}
      {backIcon && <ArrowLeftIcon onClick={() => history.goBack()} />}
    </DefaultIcon>
    <ThemedTypography variant="title" noWrap>
      {formattedTitle && <FormattedMessage id={formattedTitle} />}
      {title}
    </ThemedTypography>
    {editIcon && (
      <DefaultIcon fill="default">
        <PencilIcon />
      </DefaultIcon>
    )}
    {saveButton && (
      <StyledActionButton
        color="primary"
        onClick={saveAction}
        disabled={isLoading}
      >
        <FormattedMessage id="save" />
      </StyledActionButton>
    )}
  </StyledToolbar>
);

// PropTypes
CloseSaveToolbar.propTypes = {
  title: PropTypes.string,
  formattedTitle: PropTypes.string,
  closeIcon: PropTypes.bool,
  backIcon: PropTypes.bool,
  saveButton: PropTypes.bool,
  saveAction: PropTypes.func,
  editIcon: PropTypes.bool
};

export default withRouter(CloseSaveToolbar);
