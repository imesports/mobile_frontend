import React from "react";
import { Redirect } from "react-router-dom";
import { Route } from "react-router";
import { connect } from "react-redux";

const PublicRoute = ({ token, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      token ? (
        <Redirect
          to={{
            pathname: "/home",
            state: { from: props.location }
          }}
        />
      ) : (
        <Component {...props} />
      )
    }
  />
);

const mapStateToProps = state => ({
  token: state.user.token
});

export default connect(mapStateToProps)(PublicRoute);
