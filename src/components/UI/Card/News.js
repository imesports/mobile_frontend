// IMPORT libs
import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Card, { CardHeader, CardMedia } from "material-ui/Card";
import Typography from "material-ui/Typography";
// IMPORT icons
import DotsVerticalIcon from "mdi-react/DotsVerticalIcon";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { DefaultIcon } from "components/UI/IconFormatter";
import { FormInputSpacing } from "components/UI/Wrapper";
import CardWrapper from "components/UI/Wrapper/Card";
import CardContent from "components/UI/Card/Content";
// Custom Styles
const NoDecorA = styled.a`
   {
    text-decoration: none;
  }
  :hover {
    text-decoration: underline;
  }
`;

const StyledCardContent = styled(CardContent)`
  &&& {
    padding-top: 0;
  }
`;

const StyledCardMedia = styled(CardMedia)`
  height: ${props => props.theme.cardMediaSize.large}px;
`;
// Component: Stateless
const NewsCard = ({
  newsAbstract,
  sourceImage,
  sourceLink,
  sourceName,
  newsImage,
  newsTitle,
  newsTime,
  newsLink
}) => (
  <CardWrapper>
    <Card>
      <CardHeader
        avatar={
          <CustomSizeCircleAvatar
            size="large"
            src={sourceImage}
            title={sourceName}
          />
        }
        action={
          <DefaultIcon fill="iconColor">
            <DotsVerticalIcon />
          </DefaultIcon>
        }
        title={sourceName}
        subheader={
          <Typography variant="caption">
            {newsTime} &#x25AA; {sourceLink}
          </Typography>
        }
      />
      <NoDecorA
        alt={newsTitle}
        href={newsLink}
        target="_blank"
        rel="noopener noreferrer"
      >
        <StyledCardContent>
          <FormInputSpacing>
            <Typography variant="title">{newsTitle}</Typography>
          </FormInputSpacing>

          <Typography>{newsAbstract}</Typography>
        </StyledCardContent>
        <StyledCardMedia image={newsImage} />
      </NoDecorA>
    </Card>
  </CardWrapper>
);

// PropTypes
NewsCard.propTypes = {
  newsAbstract: PropTypes.string.isRequired,
  sourceImage: PropTypes.string.isRequired,
  sourceName: PropTypes.string.isRequired,
  sourceLink: PropTypes.string.isRequired,
  newsImage: PropTypes.string.isRequired,
  newsTitle: PropTypes.string.isRequired,
  newsTime: PropTypes.string.isRequired,
  newsLink: PropTypes.string.isRequired
};

export default NewsCard;
