import styled from "styled-components";

export const PublicWrapper = styled.div`
  margin-top: ${props => props.theme.headerSize.publicHeader}px;
`;

export const CloseSaveHeaderWrapper = styled.div`
  margin-top: ${props => props.theme.headerSize.commonToolbar}px;
`;

export const LoggedInWrapper = styled.div`
   {
    margin: auto;
    margin-top: ${props =>
      props.toolbar
        ? props.theme.headerSize.commonToolbar
        : props.theme.headerSize.loggedInHeader}px;
    @media screen and (min-width: 600px) {
      max-width: 80%;
    }
  }
`;

export const SocialNetworksWrapper = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const LogoWrapper = styled.div`
   {
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1;
    height: ${props => props.theme.headerSize.publicHeader}px;
  }
`;

export const BetweenCardsTextWrapper = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px;
  }
`;

export const FormInputSpacing = styled.div`
  margin-bottom: ${props => props.theme.spacing.default}px;
`;

export const FormOptionsWrapper = styled.div`
  padding-bottom: ${props => props.theme.spacing.default}px;
  display: flex;
  justify-content: center;
`;
