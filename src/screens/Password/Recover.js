// IMPORT libs
import React from "react";
// IMPORT components
import RecoverPasswordForm from "components/LoginOrSignUp/Password/Recover";
import { PublicWrapper } from "components/UI/Wrapper";
// Screen: Stateless
const ScreenRecoverPassword = props => (
  <PublicWrapper>
    <RecoverPasswordForm />
  </PublicWrapper>
);

export default ScreenRecoverPassword;
