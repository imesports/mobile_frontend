import { createMuiTheme } from "material-ui/styles";

export const MaterialTheme = props =>
  createMuiTheme({
    palette: {
      common: {
        black: "#000",
        white: "#fff"
      },
      type: props.type === "dark" ? "dark" : "light",
      primary: {
        main: props.type === "dark" ? "#FFFFFF" : "#673ab7"
      },
      secondary: {
        main: props.type === "dark" ? "#673ab7" : "#673ab7"
      },
      error: {
        main: props.type === "dark" ? "#e57373" : "#d32f2f"
      }
    }
  });
