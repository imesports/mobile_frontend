// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import PropTypes from "prop-types";
// IMPORT fetch
import { gameListFetch } from "util/game";
// IMPORT material
import MenuItem from "material-ui/Menu/MenuItem";
import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { NoButtonIcon } from "components/UI/IconFormatter";
import InputWithDialog from "components/UI/Input/InputWithDialog";
import { FormInputSpacing } from "components/UI/Wrapper";
import {
  CreateFormChooseAvatarPosition,
  CreateFormDontHaveGameAlert,
  CreateFormAvatarPosition,
  CreateFormStyledButton,
  CreateFormIconAlert
} from "components/Team/Wrapper";
// IMPORT images
import DefaultProfileAvatar from "imgs/default/profile_avatar.jpg";
// IMPORT icons
import InformationIcon from "mdi-react/InformationIcon";

// Component: Stateful
class CreateTeamForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      avatar_preview: props.avatar || DefaultProfileAvatar,
      localLoading: false,
      gameList: null
    };
  }

  componentDidMount() {
    if (this.props.token && this.props.userId && !this.state.gameList) {
      this.getGamesList(this.props.token);
    }
  }

  render() {
    const languagelist = [
      {
        value: "en-US",
        name: "en"
      },
      {
        value: "pt-BR",
        name: "pt-br"
      },
      {
        value: "es-ES",
        name: "es"
      }
    ];

    const objectiveslist = [
      {
        value: "FOR_FUN",
        name: "for-fun"
      },
      {
        value: "AMATEUR",
        name: "amateur"
      },
      {
        value: "PROFESSIONAL",
        name: "professional"
      }
    ];

    const {
      verifyUserHasGame,
      loading = false,
      handleChange,
      playerGames,
      objectives,
      activeStep,
      location,
      language,
      name,
      tag,
      game,
      intl
    } = this.props;

    const { avatar_preview, gameList, localLoading } = this.state;

    switch (activeStep) {
      case 0:
        return (
          <div>
            <CreateFormChooseAvatarPosition>
              <CreateFormAvatarPosition>
                <CustomSizeCircleAvatar
                  src={avatar_preview}
                  size="xxxl"
                  id="create-team-avatar"
                />
              </CreateFormAvatarPosition>
              <CreateFormStyledButton
                color="primary"
                onClick={() =>
                  document.getElementById("upload-team-avatar-picture").click()
                }
              >
                <FormattedMessage id="choose-picture" />
              </CreateFormStyledButton>
            </CreateFormChooseAvatarPosition>
            <TextField
              disabled={loading}
              id="create_team-team_name"
              fullWidth
              value={name}
              onChange={e => handleChange("name", e.target.value)}
              label={intl.formatMessage({ id: "team-name" })}
              placeholder={intl.formatMessage({
                id: "team-name.placeholder"
              })}
            />
            <FormInputSpacing />
            <InputWithDialog
              disabled={loading}
              id="create_team-team_tag"
              stateName="tag"
              label="create-team.tag"
              placeholder="create-team.tag-placeholder"
              value={tag}
              handleChange={e => handleChange("tag", e.target.value)}
              dialogTitle="create-team.tag-dialog"
              dialogText="create-team.tag-dialog-text"
              dialogButtonText="create-team.tag-dialog-button-text"
              maxLength="5"
            />
            <FormInputSpacing />
            <TextField
              id="create_team-team_game"
              select
              label={
                localLoading
                  ? intl.formatMessage({ id: "game-loading" })
                  : intl.formatMessage({ id: "game" })
              }
              fullWidth
              value={game}
              onChange={e => handleChange("game", e.target.value)}
              disabled={localLoading || loading}
            >
              {gameList ? (
                gameList.map(option => (
                  <MenuItem
                    key={`CreateTeamGame ${option.name}`}
                    value={option.id}
                  >
                    <FormattedMessage id={option.name} />
                  </MenuItem>
                ))
              ) : (
                <MenuItem value="" />
              )}
            </TextField>
            {game &&
              !verifyUserHasGame(game, playerGames) && (
                <CreateFormDontHaveGameAlert>
                  <CreateFormIconAlert>
                    <NoButtonIcon size="small" fill="primary">
                      <InformationIcon />
                    </NoButtonIcon>
                  </CreateFormIconAlert>
                  <Typography variant="caption">
                    <FormattedMessage id="create-team.dont-have-game" />
                  </Typography>
                </CreateFormDontHaveGameAlert>
              )}
            <FormInputSpacing />
            <InputWithDialog
              disabled={loading}
              id="create_team-team_location"
              stateName="location"
              label="location"
              placeholder="location-placeholder"
              value={location}
              handleChange={e => handleChange("location", e.target.value)}
              dialogTitle="create-team.location-dialog"
              dialogText="create-team.location-dialog-text"
              dialogButtonText="create-team.location-dialog-button-text"
            />

            <input
              style={{ display: "none" }}
              id="upload-team-avatar-picture"
              type="file"
              accept="image/*"
              onChange={e => {
                const fileList = e.target.files;
                let file = null;
                for (let i = 0; i < fileList.length; i++) {
                  if (fileList[i].type.match(/^image\//)) {
                    file = fileList[i];
                    break;
                  }
                }
                if (file !== null) {
                  handleChange("avatar", file);
                  this.setState({
                    avatar_preview: URL.createObjectURL(file)
                  });
                }
              }}
            />
          </div>
        );
      case 1:
        return (
          <div>
            <TextField
              disabled={loading}
              id="create_team-team_language"
              select
              label={intl.formatMessage({ id: "language" })}
              fullWidth
              value={language}
              onChange={e => handleChange("language", e.target.value)}
            >
              {languagelist.map(option => (
                <MenuItem
                  key={`CreateTeamFormLanguage ${option.value}`}
                  value={option.value}
                >
                  <FormattedMessage id={option.name} />
                </MenuItem>
              ))}
            </TextField>
            <FormInputSpacing />
            <TextField
              disabled={loading}
              id="create_team-team_objectives"
              select
              label={intl.formatMessage({ id: "objectives" })}
              fullWidth
              value={objectives}
              onChange={e => handleChange("objectives", e.target.value)}
            >
              {objectiveslist.map(option => (
                <MenuItem
                  key={`CreateTeamFormObjectives ${option.value}`}
                  value={option.value}
                >
                  <FormattedMessage id={option.name} />
                </MenuItem>
              ))}
            </TextField>
          </div>
        );
      default:
        return null;
    }
  }

  getGamesList = token => {
    this.setState({ localLoading: true });
    gameListFetch(token).then(res => {
      if (res.ok) {
        res.json().then(gameList => {
          this.setState({ gameList: gameList });
          this.setState({ localLoading: false });
        });
      } else {
        // Treatment of Error
      }
    });
  };
}
// PropTypes
CreateTeamForm.propTypes = {
  activeStep: PropTypes.number.isRequired,
  verifyUserHasGame: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  playerGames: PropTypes.array.isRequired,
  objectives: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  tag: PropTypes.string.isRequired,
  intl: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  game: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default injectIntl(CreateTeamForm);
