// IMPORT libs
import React from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT redux actions
import { logoutUser } from "reducers/user/actions";
import { swapTheme } from "reducers/app/actions";
// IMPORT material
import List, { ListItem, ListItemIcon, ListItemText } from "material-ui/List";
import Divider from "material-ui/Divider";
// IMPORT components
import { DefaultIcon } from "components/UI/IconFormatter";
// IMPORT from same folder
import { PrimaryTypography } from "./Typography";
import { DrawerContentWrapper } from "./Wrapper";
// Custom Styles
const StyledListItem = styled(ListItem)`
  && {
    padding: ${props => props.theme.spacing.small}px
      ${props => props.theme.spacing.default}px;
  }
`;

const MenuCategoryWrapper = styled.div`
  && {
    padding: ${props => props.theme.spacing.small}px
      ${props => props.theme.spacing.default}px;
  }
`;
// Component: Stateless
const DrawerList = ({
  swapMaterialTheme,
  appComponent,
  toggleDrawer,
  user_id,
  logout,
  router,
  intl,
  data
}) => {
  const drawerListItem = subcontent => (
    <StyledListItem
      button
      onClick={() => toggleDrawer("showMobileDrawer", false)}
    >
      <ListItemIcon>
        <DefaultIcon
          fill={
            subcontent.link === router.location.pathname ? "primary" : "default"
          }
        >
          <subcontent.icon />
        </DefaultIcon>
      </ListItemIcon>
      <ListItemText
        inset
        primary={intl.formatMessage({ id: subcontent.name })}
      />
    </StyledListItem>
  );

  const getFuncFromList = item => {
    switch (item) {
      case "swap-theme":
        swapMaterialTheme();
        appComponent.forceUpdate();
        break;
      default:
        logout();
        break;
    }
  };

  const getProperLink = link => {
    switch (link) {
      case "/player":
        return `${link}/${user_id}`;
      default:
        return link;
    }
  };

  return (
    <DrawerContentWrapper>
      {data.map((content, index) => (
        <div key={content.id}>
          <List>
            {content.listCategoryName && (
              <MenuCategoryWrapper>
                <PrimaryTypography variant="button">
                  <b>
                    <FormattedMessage id={content.listCategoryName} />
                  </b>
                </PrimaryTypography>
              </MenuCategoryWrapper>
            )}
            {content.listCategoryContent.map(
              subcontent =>
                subcontent.link === "function" ? (
                  // eslint-disable-next-line
                  <a
                    href="#"
                    onClick={() => getFuncFromList(subcontent.name)}
                    key={subcontent.name}
                  >
                    {drawerListItem(subcontent)}
                  </a>
                ) : (
                  <Link
                    to={getProperLink(subcontent.link)}
                    key={subcontent.name}
                  >
                    {drawerListItem(subcontent)}
                  </Link>
                )
            )}
          </List>
          <Divider />
        </div>
      ))}
    </DrawerContentWrapper>
  );
};

// PropTypes
DrawerList.propTypes = {
  data: PropTypes.array.isRequired
};
// Redux Store / State
const mapStateToProps = state => ({
  user_id: state.user.profile.user,
  router: state.router
});
// Redux dispatch
const mapDispatchToProps = dispatch => ({
  swapMaterialTheme: () => dispatch(swapTheme()),
  logout: () => dispatch(logoutUser())
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(DrawerList);
