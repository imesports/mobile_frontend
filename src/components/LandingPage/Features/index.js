// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
import Typography from "material-ui/Typography";

// IMPORT components
import { BetweenCardsTextWrapper } from "components/UI/Wrapper";
// IMPORT from same folder
import FeaturesCard from "./Card";

// Custom Styles
const FeaturesWrapper = styled.div`
  @media screen and (min-width: 800px) {
    display: flex;
    justify-content: space-between;
    margin: 0 ${props => props.theme.spacing.xs}px;
  }
`;

// Component: Stateless
const LandingPageFeatures = ({ title, features }) => (
  <div>
    <BetweenCardsTextWrapper>
      <Typography>
        <FormattedMessage id={title} />
      </Typography>
    </BetweenCardsTextWrapper>
    <FeaturesWrapper>
      {features.map(content => (
        <FeaturesCard
          key={content.title}
          cardImage={content.cardImage}
          title={content.title}
          subtitle={content.subtitle}
        />
      ))}
    </FeaturesWrapper>
  </div>
);

// PropTypes
LandingPageFeatures.propTypes = {
  title: PropTypes.string.isRequired,
  features: PropTypes.array.isRequired
};

export default LandingPageFeatures;
