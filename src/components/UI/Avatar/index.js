import Avatar from "material-ui/Avatar";
import styled from "styled-components";

export const SocialNetworkAvatar = styled(Avatar)`
  &&& {
    background-color: ${props => props.theme.social[props.color]};
    border-radius: ${props => props.theme.borderRadius.xs}px;

    svg {
      fill: ${props => props.theme.text.icon};
    }
  }
`;
