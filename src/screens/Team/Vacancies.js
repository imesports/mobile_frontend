import React from "react";

import { LoggedInWrapper } from "components/UI/Wrapper";

const TeamProfileVacancies = props => {
  return (
    <LoggedInWrapper>
      <div>Profile Vacancies</div>
    </LoggedInWrapper>
  );
};

export default TeamProfileVacancies;
