// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
// IMPORT icons
import PlusIcon from "mdi-react/PlusIcon";
// IMPORT components
import {
  StyledButton,
  Menu,
  MenuItem,
  StyledCard,
  FABIcon,
  BlurBackground
} from "components/UI/Wrapper/FABWrapper";
import { NoButtonIcon } from "components/UI/IconFormatter";
// Component: Stateful
class PlusFABWithList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menuOpen: false
    };
  }
  render() {
    const { list, history } = this.props;
    return (
      <div>
        <BlurBackground
          show={this.state.menuOpen}
          onClick={this.toggleMenu()}
        />
        <StyledButton color="primary" variant="fab" onClick={this.toggleMenu()}>
          <FABIcon show={this.state.menuOpen}>
            <NoButtonIcon fill="fabIcon" size="sm">
              <PlusIcon />
            </NoButtonIcon>
          </FABIcon>
        </StyledButton>
        <Menu show={this.state.menuOpen}>
          {list.map((content, index) => (
            <MenuItem key={index} onClick={() => history.push(content.goTo)}>
              <StyledCard>
                <Typography variant="body2">
                  <FormattedMessage id={content.title} />
                </Typography>
              </StyledCard>
              <Button color="primary" variant="fab" mini>
                <NoButtonIcon fill="fabIcon" size="sm">
                  {content.icon}
                </NoButtonIcon>
              </Button>
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
  }

  toggleMenu = () => event => {
    this.setState(prevState => ({
      menuOpen: !prevState.menuOpen
    }));
  };
}
// PropTypes
PlusFABWithList.propTypes = {
  list: PropTypes.array.isRequired
};

export default withRouter(PlusFABWithList);
