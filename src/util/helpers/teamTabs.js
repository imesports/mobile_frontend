// IMPORT icons
// import AccountMultiplePlusIcon from "mdi-react/AccountMultiplePlusIcon";
import CalendarPlusIcon from "mdi-react/CalendarPlusIcon";
import InformationIcon from "mdi-react/InformationIcon";

export const standard = [
  {
    name: "team.info",
    icon: InformationIcon,
    value: ""
  },
  {
    name: "team.training-availability",
    icon: CalendarPlusIcon,
    value: "training-availability"
  }
  // {
  //   name: "team.vacancies",
  //   icon: AccountMultiplePlusIcon,
  //   value: "vacancies"
  // }
];
