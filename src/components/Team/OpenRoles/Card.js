// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import Dialog from "material-ui/Dialog";
// IMPORT icons
import MagnifyPlusOutlineIcon from "mdi-react/MagnifyPlusOutlineIcon";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { ThemedIconButton } from "components/UI/Button";
// IMPORT from same folder
import { RoleWrapper, DescriptionWrapper, RequisitesItem } from "./Wrapper";
import OpenRoleDescriptionModalContent from "./DescriptionModalContent";
// Custom Style
const GameCardApplyIcon = styled(ThemedIconButton)`
  svg {
    width: ${props => props.theme.iconSize.large}px;
    height: ${props => props.theme.iconSize.large}px;
  }
`;

// Component: Stateful
class OpenRoleCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  render() {
    const { avatar, role, data, teamInfo } = this.props;
    const defaultDescription = data.slice(0, 3);
    return (
      <div>
        <RoleWrapper flexStart={false}>
          <RoleWrapper flexStart={true}>
            <CustomSizeCircleAvatar size="xxl" src={avatar} />
            <DescriptionWrapper>
              {defaultDescription.map(item => (
                <RequisitesItem key={item.label}>
                  <FormattedMessage id={item.label} />:{" "}
                  {Array.isArray(item.description)
                    ? item.description.join(", ")
                    : item.description}
                </RequisitesItem>
              ))}
            </DescriptionWrapper>
          </RoleWrapper>
          <GameCardApplyIcon onClick={this.handleOpen}>
            <MagnifyPlusOutlineIcon />
          </GameCardApplyIcon>
        </RoleWrapper>
        <Dialog open={this.state.open} onClose={this.handleClose}>
          <OpenRoleDescriptionModalContent
            role={role}
            avatar={avatar}
            data={data}
            handleClose={this.handleClose}
            teamInfo={teamInfo}
          />
        </Dialog>
      </div>
    );
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
}

export default OpenRoleCard;
