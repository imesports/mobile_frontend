import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { Route } from "react-router";
import { connect } from "react-redux";

import {
  getProfile,
  getPlayerGames,
  getPlayerTeams
} from "reducers/user/actions";

class PrivateRoute extends Component {
  componentDidMount() {
    const {
      userProfile,
      token,
      getProfile,
      getPlayerGames,
      getPlayerTeams
    } = this.props;
    if (
      Object.keys(userProfile).length === 0 &&
      userProfile.constructor === Object &&
      token
    ) {
      getProfile({ token });
      getPlayerGames(token);
      getPlayerTeams(token);
    }
  }

  render() {
    const { token, component: Component, ...rest } = this.props;

    return (
      <Route
        {...rest}
        render={props =>
          token ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    );
  }
}

const mapStateToProps = state => ({
  userProfile: state.user.profile,
  token: state.user.token
});

const mapDispatchToProps = dispatch => ({
  getProfile: token => dispatch(getProfile(token)),
  getPlayerGames: token => dispatch(getPlayerGames(token)),
  getPlayerTeams: token => dispatch(getPlayerTeams(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
