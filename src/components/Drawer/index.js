// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
// IMPORT material
import SwipeableDrawer from "material-ui/SwipeableDrawer";
// IMPORT from same folder
import DrawerHeader from "./Header";
import DrawerFooter from "./Footer";
import DrawerList from "./List";
// Component: Stateless
const MobileDrawer = props => {
  const {
    userProfile,
    list,
    showMobileDrawer,
    toggleDrawer,
    appComponent
  } = props;
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

  return (
    <SwipeableDrawer
      open={showMobileDrawer}
      onOpen={() => toggleDrawer("showMobileDrawer", true)}
      onClose={() => toggleDrawer("showMobileDrawer", false)}
      disableBackdropTransition={!iOS}
      disableDiscovery={iOS}
    >
      <DrawerHeader
        background_image={userProfile.background_image}
        avatar={userProfile.avatar}
        first_name={userProfile.first_name}
        last_name={userProfile.last_name}
        display_name={userProfile.display_name}
        email={userProfile.email}
      />
      <DrawerList
        appComponent={appComponent}
        toggleDrawer={toggleDrawer}
        data={list}
      />
      <DrawerFooter />
    </SwipeableDrawer>
  );
};
// PropTypes
MobileDrawer.propTypes = {
  showMobileDrawer: PropTypes.bool.isRequired,
  appComponent: PropTypes.object.isRequired,
  userProfile: PropTypes.object.isRequired,
  toggleDrawer: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired
};
export default MobileDrawer;
