import styled from "styled-components";

export const NoGameSelectedMessageWrapper = styled.div`
   {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding-top: ${props => props.theme.spacing.large}px;
  }
`;

export const SelectedGameHeaderWrapper = styled.div`
   {
    display: flex;
    align-items: center;
    width: 100%;
    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const GameNameWrapper = styled.div`
   {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-left: ${props => props.theme.spacing.default}px;
  }
`;

export const GameDescriptionWrapper = styled.div`
   {
    display: flex;
    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const IntegrationRecommendationWrapper = styled.div`
   {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;
