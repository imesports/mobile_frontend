import React from "react";
// IMPORT components
import ResetPasswordForm from "components/LoginOrSignUp/Password/Reset";
import { PublicWrapper } from "components/UI/Wrapper";
// Screen: Stateless
const ScreenResetPassword = props => (
  <PublicWrapper>
    <ResetPasswordForm />
  </PublicWrapper>
);

export default ScreenResetPassword;
