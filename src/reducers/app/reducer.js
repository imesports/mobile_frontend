// Imports constants
import CONSTANTS from "appConstants";
import { validateLocale } from "util/helper";
import * as types from "./actionTypes";

// Get locale from browser, if none, set to default
const locale =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.userLanguage ||
  CONSTANTS.DEFAULT_LOCALE;

// Check if navigator language is Portuguese or Spanish, if not, set locale to en-US
const language =
  locale.includes("pt") || locale.includes("es")
    ? locale.substring(0, 2)
    : CONSTANTS.DEFAULT_LOCALE.substring(0, 2);
// const region = navLocale.includes('BR') ? navLocale.substring(3, 5) : DEFAULT_REGION.substring(3, 5);

// Get Theme from localStorage
const theme_color = localStorage.getItem(CONSTANTS.THEME_COLOR);

const initialState = {
  language,
  locale: validateLocale(locale),
  theme: theme_color || "light",
  is_loading: false,
  gameListLoading: false,
  teamListLoading: false
};

function appReducer(state = initialState, action) {
  switch (action.type) {
    case types.CHANGE_LOCALE:
      return { ...state, locale: action.locale };

    case types.LOADING:
      return { ...state, is_loading: action.loading };

    case types.GAME_LIST_LOADING:
      return { ...state, gameListLoading: action.gameListLoading };

    case types.TEAM_LIST_LOADING:
      return { ...state, teamListLoading: action.teamListLoading };

    case types.SWAP_THEME:
      let { theme } = state;
      const newTheme = theme === "light" ? "dark" : "light";
      localStorage.setItem(CONSTANTS.THEME_COLOR, newTheme);
      return { ...state, theme: newTheme };

    default:
      return state;
  }
}

export default appReducer;
