import CONSTANTS from "appConstants";

export const createTeamFetch = (token, body) =>
  fetch(`${CONSTANTS.BACKEND_URL}/teams/`, {
    method: "POST",
    headers: {
      Authorization: `JWT ${token}`
    },
    body
  });

export const playerTeamsFetch = (token, userId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-profile/${userId}/teams/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });

export const basicTeamInfo = (token, teamId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/teams/${teamId}/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });
