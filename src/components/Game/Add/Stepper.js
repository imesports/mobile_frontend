// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
// IMPORT material
import Button from "material-ui/Button";
// IMPORT Redux Actions
import { updatePlayerGames } from "reducers/user/actions";
// IMPORT util / fetch
import { addGameFetch } from "util/game";
// IMPORT components
import DialogConfirmExitOrSave from "components/UI/Dialog/ConfirmExitOrSave";
import AppHeaderCloseSave from "components/AppHeader/CloseOrBackSaveOrEdit";
import CardContent from "components/UI/Card/Content";
import {
  StepperContainerWrapper,
  StyledMobileStepper,
  SpacingPattern,
  StepperPosition
} from "components/UI/Wrapper/Stepper";
// IMPORT from same folder
import AddGameForm from "./Form";

// Component: Stateful
class AddGameStepper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      preventExitWithoutSave: false,
      loading: false,
      selectedGame: ""
    };
  }

  render() {
    const { userId, token, playerGames } = this.props;
    const { selectedGame, preventExitWithoutSave, loading } = this.state;

    return (
      <div>
        <AppHeaderCloseSave
          closeIcon
          closeIconAction={selectedGame ? this.dialogToggle(true) : null}
          formattedTitle="add-game"
          isLoading={loading}
        />
        {userId &&
          token &&
          playerGames && (
            <div>
              <StepperContainerWrapper>
                <CardContent>
                  <AddGameForm
                    loading={loading}
                    selectGame={this.selectGame}
                    selectedGame={selectedGame}
                    token={token}
                    userId={userId}
                    playerGames={playerGames}
                  />
                </CardContent>
              </StepperContainerWrapper>
              <StepperPosition>
                <StyledMobileStepper
                  variant="dots"
                  steps={0}
                  position="static"
                  activeStep={0}
                  nextButton={
                    <SpacingPattern>
                      <Button
                        disabled={!Boolean(this.state.selectedGame) || loading}
                        style={{ float: "right" }}
                        color="primary"
                        size="large"
                        onClick={this.addGame(token, userId, selectedGame)}
                      >
                        <FormattedMessage id="add-game" />
                      </Button>
                    </SpacingPattern>
                  }
                  backButton={<SpacingPattern />}
                />
              </StepperPosition>
            </div>
          )}
        <DialogConfirmExitOrSave
          description="leaving-without-save.description-games"
          saveTitle="add-game"
          showDialog={preventExitWithoutSave}
          saveAction={this.addGame(token, userId, selectedGame)}
          removeDialog={this.dialogToggle(false)}
          goToOnClose="/my-games"
        />
      </div>
    );
  }

  addGame = (token, user_id, game_id) => () => {
    this.setState({ loading: true });
    addGameFetch(token, user_id, game_id).then(response => {
      this.setState({ loading: false });
      if (response.ok) {
        response.json().then(res => this.props.updatePlayerGames(res.games));
        this.props.history.push("../my-games");
      } else {
        // Treatment of Error
      }
    });
  };

  selectGame = value => {
    this.setState({ selectedGame: value });
  };

  dialogToggle = value => () => {
    this.setState({ preventExitWithoutSave: value });
  };
}

const mapStateToProps = state => ({
  playerGames: state.user.games,
  userId: state.user.profile.user,
  token: state.user.token
});

const mapDispatchToProps = dispatch => ({
  updatePlayerGames: games => dispatch(updatePlayerGames(games))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter
)(AddGameStepper);
