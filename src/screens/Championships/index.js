// IMPORT libs
import React from "react";
// IMPORT components
import NoContent from "components/UI/Content/NoContent";
import { LoggedInWrapper } from "components/UI/Wrapper";
// IMPORT img
import Sorry from "../../imgs/delete_later/sorry.png";

const MyTeams = props => (
  <LoggedInWrapper>
    <NoContent
      title="coming-soon"
      img={Sorry}
      callToAction="coming-soon-description"
    />
  </LoggedInWrapper>
);

export default MyTeams;
