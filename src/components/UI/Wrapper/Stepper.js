import MobileStepper from "material-ui/MobileStepper";
import styled from "styled-components";

export const StepperContainerWrapper = styled.div`
  position: absolute;

  padding-top: ${props => props.theme.headerSize.commonToolbar}px;
  padding-bottom: ${props => props.theme.stepperSize.default}px;

  min-height: 100%;
  width: 100%;

  background: ${props => props.theme.background.stepper};
`;

export const StyledMobileStepper = styled(MobileStepper)`
  && {
    background: ${props => props.theme.background.stepper};
    border-top-style: solid;
    border-top-width: 1px;
    border-top-color: ${props => props.theme.background.innerCard};
  }
`;

export const SpacingPattern = styled.div`
   {
    flex: 1;
  }
`;

export const StepperPosition = styled.div`
   {
    position: fixed;
    bottom: 0;
    width: 100%;
  }
`;
