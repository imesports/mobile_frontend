// IMPORT libs
import React from "react";
// IMPORT components
import { PublicWrapper } from "components/UI/Wrapper";
import LoginForm from "components/LoginOrSignUp/Form";
// Screen: Stateless
const ScreenSignUp = props => (
  <PublicWrapper>
    <LoginForm
      login={false}
      title="landing-page.sign-up"
      subtitle="sign-up-page.message"
      userLabel="sign-up-page.user"
      userPlaceholder="sign-up-page.user-placeholder"
      facebookText="sign-up-page.facebook"
      googleText="sign-up-page.google"
      footerText="sign-up-page.already-have-account"
      termsOfUse="sign-up-page.terms-of-use"
      linkTo="/login"
    />
  </PublicWrapper>
);

export default ScreenSignUp;
