// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import FacebookBoxIcon from "mdi-react/FacebookBoxIcon";
import LinkedinBoxIcon from "mdi-react/LinkedinBoxIcon";
import TwitterBoxIcon from "mdi-react/TwitterBoxIcon";
import InstagramIcon from "mdi-react/InstagramIcon";
import HeartIcon from "mdi-react/HeartIcon";
// IMPORT components
import { NoButtonIcon } from "components/UI/IconFormatter";
// IMPORT from same folder
import { DrawerContentWrapper } from "./Wrapper";
import FooterDrawerItem from "./Item/Footer";
// Custom Styles
const SocialNetworksWrapper = styled.div`
   {
    display: flex;
    justify-content: space-around;
    padding: ${props => props.theme.spacing.default}px;
  }
`;

const MadeInWrapper = styled.div`
   {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 0 ${props => props.theme.spacing.default}px;
  }
`;

const MadeWithLoveWrapper = styled.div`
   {
    display: flex;
    align-items: center;
  }
`;
// Component: Stateless
const DrawerFooter = props => (
  <DrawerContentWrapper>
    <FooterDrawerItem
      formattedText="company-page"
      goTo="//company.imesports.gg"
      externalLink
    />
    <FooterDrawerItem formattedText="terms-of-use" goTo="/terms" />
    <FooterDrawerItem formattedText="privacy-policy" goTo="/privacy" />
    <SocialNetworksWrapper>
      <a
        href="//facebook.com/ImeSportsGG"
        target="_blank"
        rel="noopener noreferrer"
      >
        <NoButtonIcon fill="default" size="medium">
          <FacebookBoxIcon />
        </NoButtonIcon>
      </a>
      <a
        href="//twitter.com/ImeSportsGG"
        target="_blank"
        rel="noopener noreferrer"
      >
        <NoButtonIcon fill="default" size="medium">
          <TwitterBoxIcon />
        </NoButtonIcon>
      </a>
      <a
        href="//www.instagram.com/ImeSportsgg/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <NoButtonIcon fill="default" size="medium">
          <InstagramIcon />
        </NoButtonIcon>
      </a>
      <a
        href="//linkedin.com/company/imesports/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <NoButtonIcon fill="default" size="medium">
          <LinkedinBoxIcon />
        </NoButtonIcon>
      </a>
    </SocialNetworksWrapper>
    <MadeInWrapper>
      <Typography variant="caption">
        <b>
          <FormattedMessage id="copyright" />
        </b>
      </Typography>
      <MadeWithLoveWrapper>
        <Typography variant="caption">
          <FormattedMessage id="created-with" />
        </Typography>
        <NoButtonIcon fill="default" size="small" style={{ margin: 3 }}>
          <HeartIcon />
        </NoButtonIcon>
        <Typography variant="caption">
          <FormattedMessage id="in-floripa" />
        </Typography>
      </MadeWithLoveWrapper>
    </MadeInWrapper>
  </DrawerContentWrapper>
);

export default DrawerFooter;
