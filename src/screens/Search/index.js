// IMPORT libs
import React, { Component } from "react";
// IMPORT components
import TeamOpenPositions from "components/Team/AllOpenPositions";
import { LoggedInWrapper } from "components/UI/Wrapper";
// IMPORT img
import SniperIcon from "../../imgs/delete_later/sniper.png";
import RiflerIcon from "../../imgs/delete_later/rifler.png";
// Screen: Stateful
class SearchScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openPositions: {
        size: 4,
        vacancies: [
          {
            teamName: "SK Gaming",
            teamImg:
              "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAM1BMVEX///8DAABCQEDAv7+Afn4TEBDQz8/v7++gn59iYGAyMDAjICDf39+Qj4+wr69ycHBSUFA9dXpzAAAFJ0lEQVR4nO2d13LjMAxF1asl+f+/NrJ3tszEFyQoQACzvO+xfUKisAFVVVRUVFRUVFT0f2lov2m0/k0f1E7ds2nqOzRpMQzbvN5C8NY66FDsd0KcalSmYzv3t1LU9aFAMU73jsWpflfA6O4ejLpe5M3DAqN+ypvHZoBRb+IYw2KA0bfSGONhgFEv4tPKZDjqWRqj2iwwevGkZJwtOOSTktFkWsl73eH2SP5SJ41RDRbBQ97r2nAsj5/BIe91TTjkve7prwzsXCHXZfrdZu66/ftOA1caS8HoOLjOk7x5yikyL1kPpX0BKQ1xE0phGSqrKENv5AOXuCLWH0sGGFUbxOjlF6EaCk6sxrOf+qsuxCGfnapoDKQmGltmKgqEwt555PijB82hkGUriR4Q+T0aLdED0mczHnQszMY+Qi4rF391aqI4Mokfb1HLqcb6xzFEmXpGhk5nJ6l54tCQ0vEfxMxKnViBzRidE3RqZiX+50w4qJV64saZDUf1xF+ZZulGHBX+2rQBseIg9k6SLMSKgwjrS8rHmXEQCWPKl9pxVPjuVcIqxJAD2/qT/1mWHJXg15py4H05dhAx5cAgbJ9ly4G9LzcaGnPgHJ6ZwFtzYBDexrs5BwZhfYo9hwyIAw4IwlkbeuCQAKE5FG6ZfNR1kADHXfuUCCQ603LCAUFiNxi9cFwFccNxEcQPxzUQRxyXQDxxXAFxxXHB/friSA+IzjiSQbxxpIK440gE8ceRBuKQIwnEI0cKiEuOBBCfHHwQpxxskAe9Pl+b5ui69w3tq7/sw3Pyt8BWLhckfIfzn894dhcujKPzDpAFaoK8lXz73BvIS8uWwOIR5MXC3t0SAkHrkVSQ06F1PHtxC8JFcQxyWj7jOp5rEE4tB+cg8Xe6vYNE7/35B4k8nswAJI4kB5AokixAYkjyAIk4j8sEJHyrLReQ4FuOXECC4SQbkND1IybIBC59I2MM3BXngAR2ZZkg8mrbrWui3sXT+xPmIL/0mMLFxegzGCcgL7Wh9/GklTgCOceFRiELtLkCCRRT6qm/dAZC1yGiFlneQMjXmlTu6A+EeNixEn/lEIQowkD4LYcgRF5DZPMeQfBLAsJIXIKk3Pd2CYIrSuA/8QkCn9Zha/cJAl9y4QzYJwh8b4PdllMQFBTxz3IKwr/g5hQEOWAcSJyCIGvHC3enIOhRWn4gwG1hEOTnrEHAPxiDAI4CIiUuCMwFrOu0gMfNEAQmzNbl40BEhCAwzcwNBD5Pty4txQWBi8o7f/QncUFQGEmqeyApJgis1pLwzF5WTBD4GNo6jHBB4PaktdPigsDdCmunxQSBcd3c1pkgMBxq9LzhiQcCZ5Z1psUEwXWAzE0EnYN/nirQZ5lHEZ5GOCD3vI0XE66KZz+zOMIFMDObWfjQ0d5ncYRdFnXk6FD4ZD6P6uK/hSuX9VmZ+g457DN4joi3aFkNCPWmLqcBoThyGhDyjWNG2clEceRTQTnQly2bEuM7fZ8zF0tvAxdt7ZfqUdpD94WzqF3/6MKXhN17rHE/YlpnKbRQk9PQdnNk/y8dz3vQl+PjxOrEptJsw6AnqkoNfjL2KnFo9LQz6ImqwWHRS1SDw6LVrkZfcNZjHSkOeX/V/pBWoiYNteXzkodF52MF8zAIHnV9iJuHSUNthcaLFn3aNRovGgQPbv2UGI1Ek5WMMAyCx7pppOzBppXC6meV6xk35yTrrHQatd84rZZ5U9sluWNara9j9m43v+5TVFRUVFRU5ERfN7REudWDddMAAAAASUVORK5CYII=",
            address: {
              city: "Florianopolis",
              state: "SC",
              country: "Brasil"
            },
            game: "pubg",
            positions: [
              {
                avatar: SniperIcon,
                role: "position.sniper",
                requirements: [
                  {
                    label: "hours-played",
                    description: "1200 h"
                  },
                  {
                    label: "maps",
                    description: ["Erangel", "Miramar"]
                  },
                  {
                    label: "age",
                    description: "18"
                  },
                  {
                    label: "rank",
                    description: "Supreme"
                  },
                  {
                    label: "training",
                    description: "8 h"
                  }
                ],
                training: {}
              },
              {
                avatar: RiflerIcon,
                role: "position.rifler",
                requirements: [
                  {
                    label: "hours-played",
                    description: "1000 h"
                  },
                  {
                    label: "maps",
                    description: "Erangel"
                  },
                  {
                    label: "age",
                    description: "18"
                  }
                ]
              },
              {
                avatar: RiflerIcon,
                role: "position.rifler",
                requirements: [
                  {
                    label: "hours-played",
                    description: "1000 h"
                  },
                  {
                    label: "maps",
                    description: "Miramar"
                  },
                  {
                    label: "age",
                    description: "18"
                  }
                ]
              }
            ]
          },
          {
            teamName: "Cloud9 Gaming",
            teamImg:
              "https://ih0.redbubble.net/image.430497876.0935/flat,900x900,070,f.jpg",
            address: {
              city: "Blumenau",
              state: "SC",
              country: "Brasil"
            },
            game: "pubg",
            positions: [
              {
                avatar: SniperIcon,
                role: "position.sniper",
                requirements: [
                  {
                    label: "hours-played",
                    description: "1200 h"
                  },
                  {
                    label: "maps",
                    description: "Erangel"
                  },
                  {
                    label: "age",
                    description: "18"
                  }
                ]
              }
            ]
          }
        ]
      }
    };
  }

  render() {
    return (
      <LoggedInWrapper>
        <TeamOpenPositions openPositions={this.state.openPositions} />
      </LoggedInWrapper>
    );
  }
}

export default SearchScreen;
