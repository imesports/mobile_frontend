import Divider from "material-ui/Divider";
import styled from "styled-components";

export const ThickDivider = styled(Divider)`
  && {
    height: 2px;
  }
`;

export const DividerDefaultMargin = styled(Divider)`
  && {
    margin: ${props => props.theme.spacing.default}px 0px;
  }
`;
