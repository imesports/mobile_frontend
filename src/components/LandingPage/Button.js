// IMPORT libs
import Button from "material-ui/Button";
import styled from "styled-components";

// Custom Styles
export const SignUpButton = styled(Button)`
  &&& {
    background: ${props => props.theme.misc.signUpButton};
    color: ${props => props.theme.text.darkGrey};
    font-size: 1.2em;
    text-transform: none;

    margin: ${props => props.theme.spacing.default}px
      ${props => props.theme.spacing.default}px 0;
`;
