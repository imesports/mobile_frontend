// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
// IMPORT material
import MenuItem from "material-ui/Menu/MenuItem";
import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";
import Card from "material-ui/Card";
// IMPORT components
import PlayerProfilePersonalInfoHeader from "components/Player/Profile/PersonalInfo/Header";
import CardWrapper from "components/UI/Wrapper/Card";
import CardContent from "components/UI/Card/Content";
// IMPORT from same folder
import { LoginFormWrapper } from "./Wrapper";
// Custom Styles
const StyledCard = styled(Card)`
  width: 100%;
  max-width: 450px;

  @media screen and (min-width: 450px) {
    margin-top: ${props => props.theme.spacing.xl}px;
  }
`;

const StyledCardContent = styled(CardContent)`
  &&& {
    padding-top: 0px;
  }
`;

const CardTitle = styled.div`
   {
    padding: ${props => props.theme.spacing.medium}px;
  }
`;
// Component: Stateful
class SignUpLastStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      changeBackground: true,
      background_image_preview: props.background_image || "",
      changeAvatar: true,
      avatar_preview: props.avatar || ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      typeof nextProps.avatar === "string" &&
      this.props.avatar !== nextProps.avatar
    )
      this.setState({ avatar_preview: nextProps.avatar });
    if (
      typeof nextProps.background_image === "string" &&
      this.props.background_image !== nextProps.background_image
    )
      this.setState({ background_image_preview: nextProps.background_image });
  }

  render() {
    const {
      intl,
      first_name,
      display_name,
      last_name,
      headline,
      location,
      handleChange,
      isLoading = false
    } = this.props;

    const countries = [
      {
        value: "BR",
        label: "Brasil"
      }
    ];

    return (
      <CardWrapper>
        <LoginFormWrapper>
          <StyledCard>
            <CardTitle>
              <Typography>
                <FormattedMessage id="sign-up2.subtitle" />
              </Typography>
            </CardTitle>
            <PlayerProfilePersonalInfoHeader
              avatar={this.state.avatar_preview}
              background_image={this.state.background_image_preview}
              first_name={first_name}
              display_name={display_name}
              last_name={last_name}
              headline={headline}
              location={location}
              changeAvatar={this.state.changeAvatar}
              changeBackground={this.state.changeBackground}
            />
            <StyledCardContent>
              <TextField
                disabled={isLoading}
                name="first_name"
                id="first_name_id"
                required
                label={intl.formatMessage({ id: "first-name" })}
                fullWidth
                margin="normal"
                value={first_name}
                onChange={e => handleChange(e.target.value, "first_name")}
              />
              <TextField
                disabled={isLoading}
                name="display_name"
                id="display_name_id"
                label={intl.formatMessage({ id: "display-name" })}
                fullWidth
                margin="normal"
                value={display_name}
                onChange={e => handleChange(e.target.value, "display_name")}
              />
              <TextField
                disabled={isLoading}
                name="last_name"
                id="last_name_id"
                required
                label={intl.formatMessage({ id: "last-name" })}
                fullWidth
                margin="normal"
                value={last_name}
                onChange={e => handleChange(e.target.value, "last_name")}
              />
              <TextField
                disabled={isLoading}
                name="headline"
                id="headlineId"
                label={intl.formatMessage({ id: "headline" })}
                fullWidth
                margin="normal"
                value={headline}
                onChange={e => handleChange(e.target.value, "headline")}
              />
              <TextField
                disabled={isLoading}
                name="country"
                id="countryId"
                required
                select
                label={intl.formatMessage({ id: "country" })}
                fullWidth
                margin="normal"
                value={location}
                onChange={e => handleChange(e.target.value, "location")}
              >
                {countries.map(option => (
                  <MenuItem key={option.value} value={option.label}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>

              <input
                style={{ display: "none" }}
                id="upload-bg-picture"
                type="file"
                accept="image/*"
                onChange={e => {
                  const fileList = e.target.files;
                  let file = null;
                  for (let i = 0; i < fileList.length; i++) {
                    if (fileList[i].type.match(/^image\//)) {
                      file = fileList[i];
                      break;
                    }
                  }
                  if (file !== null) {
                    handleChange(file, "background_image");
                    this.setState({
                      changeBackground: false,
                      background_image_preview: URL.createObjectURL(file)
                    });
                  }
                }}
              />
              <input
                style={{ display: "none" }}
                id="upload-avatar-picture"
                type="file"
                accept="image/*"
                onChange={e => {
                  const fileList = e.target.files;
                  let file = null;
                  for (let i = 0; i < fileList.length; i++) {
                    if (fileList[i].type.match(/^image\//)) {
                      file = fileList[i];
                      break;
                    }
                  }
                  if (file !== null) {
                    handleChange(file, "avatar");
                    this.setState({
                      changeAvatar: false,
                      avatar_preview: URL.createObjectURL(file)
                    });
                  }
                }}
              />
            </StyledCardContent>
          </StyledCard>
        </LoginFormWrapper>
      </CardWrapper>
    );
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
}

export default injectIntl(SignUpLastStep);
