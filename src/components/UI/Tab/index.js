import Tabs, { Tab } from "material-ui/Tabs";
import styled from "styled-components";

export const ThemedTabs = styled(Tabs)`
  && {
    color: ${props => props.theme.icon.default};
  }
`;

export const ThemedTab = styled(Tab)`
  && {
    flex-grow: 1;
    color: ${props =>
      props.selected ? props.theme.primary.default : props.theme.icon.default};
  }
  &&& svg {
    fill: ${props =>
      props.selected ? props.theme.primary.default : props.theme.icon.default};
  }
`;

export const WithLabelTabs = styled(Tabs)`
  && {
    color: ${props => props.theme.icon.default};
  }
`;

export const WithLabelTab = styled(Tab)`
  && {
    flex-grow: 1;
    color: ${props =>
      props.selected ? props.theme.primary.default : props.theme.icon.default};
    height: auto;
    text-transform: none;

    && span {
      font-size: 12px;
    }

    &&& svg {
      fill: ${props =>
        props.selected
          ? props.theme.primary.default
          : props.theme.icon.default};
    }
  }
`;
