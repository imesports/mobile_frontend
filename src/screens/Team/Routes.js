/* Imports from libraries */
import React, { Component } from "react";
import { Switch } from "react-router";
import { connect } from "react-redux";
/* Import Actions */
import { getTeamInfo } from "reducers/temporary/actions";
/* Import Helpers */
import { standard } from "util/helpers/teamTabs";
/* Import Material */
import { CircularProgress } from "material-ui/Progress";
/* Import Components */
import PrivateRoute from "components/Route/PrivateRoute";
import asyncComp from "components/AsyncComponent";
import TeamHeader from "components/Team/AppHeader";
/* Import Styles */
import Centralize from "components/UI/Wrapper/Centralize";

/* Import Screens */
const ScreenTeamProfileInformation = asyncComp(() =>
  import("screens/Team/Information")
);
const ScreenTeamProfileTraining = asyncComp(() =>
  import("screens/Team/Training")
);
const ScreenTeamProfileVacancies = asyncComp(() =>
  import("screens/Team/Vacancies")
);

// All /my-teams routes
class TeamProfileRoutes extends Component {
  componentDidMount() {
    // Only update Redux if Team Page is different for the previous
    if (this.props.teamInfo.id !== this.props.match.params.team_id) {
      this.props.getTeamInfo(this.props.token, this.props.match.params.team_id);
    }
  }

  render() {
    const { teamInfoLoading, teamInfo, match } = this.props;

    if (teamInfoLoading)
      return (
        <Centralize>
          <CircularProgress size={50} />
        </Centralize>
      );

    if (Object.keys(teamInfo).length === 0 && teamInfo.constructor === Object)
      return (
        // If team doesnt exist
        // TODO: ERROR PAGE
        <div>This team doesn't exist</div>
      );

    return (
      // If team exist
      <div>
        <TeamHeader {...teamInfo} tabs={standard} />
        <Switch>
          {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
          <PrivateRoute
            exact
            path={match.url}
            component={ScreenTeamProfileInformation}
          />
          <PrivateRoute
            exact
            path={`${match.url}/training-availability`}
            component={ScreenTeamProfileTraining}
          />
          <PrivateRoute
            exact
            path={`${match.url}/vacancies`}
            component={ScreenTeamProfileVacancies}
          />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.user.token,
  teamInfo: state.temporary.teamInfo,
  teamInfoLoading: state.temporary.teamInfoLoading
});

const mapDispatchToProps = dispatch => ({
  getTeamInfo: (token, teamId) => dispatch(getTeamInfo(token, teamId))
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamProfileRoutes);
