// Import constants
import * as types from "./actionTypes";

const initialState = {
  teamInfoLoading: true,
  teamInfo: {}
};

function temporaryReducer(state = initialState, action) {
  switch (action.type) {
    case types.UPDATE_TEMPORARY_TEAM_INFO:
      return { ...state, teamInfo: action.teamInfo };

    case types.TEAM_INFO_LOADING:
      return { ...state, teamInfoLoading: action.teamInfoLoading };

    default:
      return state;
  }
}

export default temporaryReducer;
