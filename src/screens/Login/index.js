// IMPORT libs
import React from "react";
// IMPORT components
import { PublicWrapper } from "components/UI/Wrapper";
import LoginForm from "components/LoginOrSignUp/Form";
// Screen: Stateless
const ScreenLogin = props => (
  <PublicWrapper>
    <LoginForm
      login={true}
      title="landing-page.log-in"
      subtitle="login-page.message"
      userLabel="login-page.user"
      userPlaceholder="login-page.user-placeholder"
      facebookText="login-page.facebook"
      googleText="login-page.google"
      footerText="login-page.dont-have-account"
      forgotPassword="landing-page.forgot-password"
      linkTo="/signup"
    />
  </PublicWrapper>
);

export default ScreenLogin;
