// IMPORT libs
import React, { Component } from "react";
// IMPORT components
import Features from "components/LandingPage/Features";
import CircleCardsList from "components/LandingPage/CircleCardsList";
import ReleaseDate from "components/LandingPage/ReleaseDate";
import Footer from "components/LandingPage/Footer";
import SignUp from "components/LandingPage/LoginOrSignUp";
import { PublicWrapper } from "components/UI/Wrapper";
// IMPORT images
import SignUpBackgroundImg from "imgs/landing-page/SignUpBackground.jpg";
import stateInitialization from "./stateInitialization";

// Screen
class LandingPageScreen extends Component {
  constructor(props) {
    super(props);

    this.state = stateInitialization;
  }

  render() {
    return (
      <PublicWrapper>
        {/* SIGN UP & LOG IN SECTION */}
        <SignUp
          backgroundImg={SignUpBackgroundImg}
          title="im-esports"
          subtitle="landing-page.join-the-best-place"
          signUp="landing-page.sign-up"
          logIn="landing-page.log-in"
        />

        {/* WHAT WE OFFER SECTION: GAMES */}
        <CircleCardsList
          title="landing-page.games-we-offer"
          cardsList={this.state.gamesList}
        />

        {/* WHAT WE OFFER SECTION: FEATURES */}
        <Features
          title="landing-page.dont-loose-the-opportunity"
          features={this.state.featuresList}
        />

        {/* RELEASE DATE SECTION */}
        <ReleaseDate
          textFirstPart="landing-page-release-date"
          textSecondPart="landing-page-release-date-2"
          buttonText="landing-page.sign-up-today"
        />

        {/* TEAM */}
        <CircleCardsList
          title="landing-page.team"
          cardsList={this.state.imeSportsTeamList}
        />

        {/* FOOTER */}
        <Footer
          subtitle="social-networks"
          socialNetworks={this.state.socialNetworks}
        />
      </PublicWrapper>
    );
  }
}

export default LandingPageScreen;
