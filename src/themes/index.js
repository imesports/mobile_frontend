/**
  All color tags are from the Material Color Palette

  https://material.io/guidelines/style/color.html#color-color-palette

  Create Themes for our aplication in this document.
  Example: PUBG theme, CSGO theme, Main theme, Dark theme...

  When creating a new Theme, use same tags as the ones in landingPageTheme.

  keywords: NewTheme, NewColor, MaterialColor, Theme, Colors
*/

const palette = {
  primary: {
    default: "#673ab7"
  },
  secondary: {
    default: "#607d8b"
  },
  background: {
    appbar: "#FFFFFF",
    footer: "#607D8B",
    body: "#ECEFF1",
    innerCard: "#F5F5F5",
    purple: "#673ab7",
    stepper: "#FFFFFF",
    black: "#000000"
  },
  text: {
    primary: "#212121",
    secondary: "#757575",
    button: "#FFFFFF",
    footer: "#FAFAFA",
    icon: "#FFFFFF",
    white: "#FFFFFF",
    black: "#000000",
    darkGrey: "#212121"
  },
  misc: {
    signUpButton: "#FFFF00"
  },
  icon: {
    default: "#9E9E9E",
    primary: "#673ab7",
    verified: "#4caf50",
    fabIcon: "#FFFFFF",
    white: "#FFFFFF",
    contrast: "#000000"
  },
  social: {
    facebook: "#3B5998",
    googleRed: "#D62D20"
  },
  game: {
    PUBG: "#ff8200"
  }
};

const paletteDark = {
  primary: {
    default: "#FFFFFF"
  },
  secondary: {
    default: "#90a4ae"
  },
  background: {
    appbar: "#212121",
    footer: "#607D8B",
    body: "#212121",
    innerCard: "#212121",
    purple: "#673ab7",
    stepper: "#424242",
    black: "#000000"
  },
  text: {
    primary: "#FFFFFF",
    secondary: "#f5f5f5",
    button: "#000000",
    footer: "#FAFAFA",
    icon: "#FFFFFF",
    white: "#FFFFFF",
    black: "#000000",
    darkGrey: "#212121"
  },
  misc: {
    signUpButton: "#FFFF00"
  },
  icon: {
    default: "#9E9E9E",
    primary: "#FFFFFF",
    verified: "#4caf50",
    fabIcon: "#000000",
    white: "#FFFFFF",
    contrast: "#FFFFFF"
  },
  social: {
    facebook: "#3B5998",
    googleRed: "#D62D20"
  },
  game: {
    PUBG: "#ff8200"
  }
};

const sizes = {
  borderRadius: {
    xs: 2
  },
  iconSize: {
    default: 48,
    xs: 12,
    small: 20,
    sm: 24,
    medium: 30,
    large: 40,
    xl: 60,
    xxl: 90,
    xxxl: 125
  },
  headerSize: {
    commonToolbar: 56,
    publicHeader: 64,
    loggedInHeader: 104
  },
  cardMediaSize: {
    medium: 100,
    large: 150,
    xl: 200,
    xxl: 300
  },
  stepperSize: {
    default: 55
  },
  fabSize: {
    default: 56,
    mini: 40
  }
};

const spacing = {
  spacing: {
    default: 16,
    xxxs: 1,
    xxs: 3,
    xs: 5,
    small: 10,
    medium: 15,
    large: 20,
    xl: 25
  }
};

export const mainTheme = ({ type }) => {
  // Use dark theme if type = 'dark', otherwise get 'light'
  const colorPallete = type === "dark" ? paletteDark : palette;

  const theme = {
    // Pallete (All Collors)
    ...colorPallete,
    // Spacing
    ...spacing,
    // Icon sizes
    ...sizes
  };

  return theme;
};
