import IconButton from "material-ui/IconButton";
import styled from "styled-components";

export const DefaultIcon = styled(IconButton)`
  &&& {
    height: calc(
      ${props => props.theme.iconSize[props.size]}px +
        ${props => props.theme.spacing.small}px
    );
    width: calc(
      ${props => props.theme.iconSize[props.size]}px +
        ${props => props.theme.spacing.small}px
    );
  }
  &&& svg {
    fill: ${props =>
      props.fill ? props.theme.icon[props.fill] : props.theme.icon.default};
    height: ${props =>
      props.size
        ? props.theme.iconSize[props.size]
        : props.theme.iconSize.sm}px;
    width: ${props =>
      props.size
        ? props.theme.iconSize[props.size]
        : props.theme.iconSize.sm}px;
  }
`;

export const NoButtonIcon = styled.div`
  &&& {
    height: ${props => props.theme.iconSize[props.size]}px;
    width: ${props => props.theme.iconSize[props.size]}px;
  }
  &&& svg {
    fill: ${props => props.theme.icon[props.fill]};
    height: ${props => props.theme.iconSize[props.size]}px;
    width: ${props => props.theme.iconSize[props.size]}px;
  }
`;
