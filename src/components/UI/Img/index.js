import styled from "styled-components";

export const LogoImg = styled.div`
   {
    fill: ${props => props.theme.text[props.color]};
    width: ${props => props.theme.cardMediaSize.xl}px;
  }
  @media screen and (max-width: 300px) {
    width: 100%;
  }
`;
