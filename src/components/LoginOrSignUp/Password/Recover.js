// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import { Link } from "react-router-dom";
// IMPORT material
import Card from "material-ui/Card";
import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";
// IMPORT components
import { FormInputSpacing, FormOptionsWrapper } from "components/UI/Wrapper";
import CardHeaderTitleSubtitle from "components/UI/Card/Header/TitleSubtitle";
import { RaisedAccentButton } from "components/UI/Button";
import CardContent from "components/UI/Card/Content";
// IMPORT from parent folder
import { LoginFormWrapper, ButtonWrapper } from "../Wrapper";
// Custom Styles
const StyledCard = styled(Card)`
  width: 100%;
  max-width: 450px;

  @media screen and (min-width: 450px) {
    margin-top: ${props => props.theme.spacing.xl}px;
  }
`;

// Component: Stateful
class LoginPasswordRecover extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      success: false
    };
  }

  render() {
    const { intl } = this.props;

    return (
      <LoginFormWrapper>
        <StyledCard>
          <CardContent>
            {this.state.success ? (
              <CardHeaderTitleSubtitle
                title="forgot-password.sucess-title"
                subtitle="forgot-password.sucess-text"
              />
            ) : (
              <div>
                <CardHeaderTitleSubtitle
                  title="forgot-password"
                  subtitle="forgot-password.subtitle"
                />

                <FormInputSpacing>
                  <TextField
                    id="user"
                    type="email"
                    fullWidth
                    onChange={this.handleChange("email")}
                    label={intl.formatMessage({ id: "login-page.user" })}
                    placeholder={intl.formatMessage({
                      id: "login-page.user-placeholder"
                    })}
                  />
                </FormInputSpacing>
                {/* FORM BUTTONS AND OPTIONS */}
                <ButtonWrapper>
                  <RaisedAccentButton onClick={this.handleClick}>
                    <FormattedMessage id="forgot-password.recovery-password" />
                  </RaisedAccentButton>
                </ButtonWrapper>
              </div>
            )}
          </CardContent>

          {/* FORM FOOTER / ADDITIONAL OPTIONS */}
          <FormOptionsWrapper>
            <Link to="/login">
              <Typography variant="caption">
                <FormattedMessage id="forgot-password.try-login" />
              </Typography>
            </Link>
          </FormOptionsWrapper>
        </StyledCard>
      </LoginFormWrapper>
    );
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleClick = () => {
    this.setState(prevState => ({
      success: !prevState.success
    }));
  };
}

export default injectIntl(LoginPasswordRecover);
