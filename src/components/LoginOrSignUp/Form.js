// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
import FacebookIcon from "mdi-react/FacebookIcon";
import Typography from "material-ui/Typography";
import GoogleIcon from "mdi-react/GoogleIcon";
import TextField from "material-ui/TextField";
import Divider from "material-ui/Divider";
import Card from "material-ui/Card";
// IMPORT redux actions
import { signUp, logIn, getProfile } from "reducers/user/actions";
// IMPORT components
import PasswordWithToggleView from "components/UI/Input/PasswordWithToggleView";
import { RaisedAccentButton, SmallAccentButton } from "components/UI/Button";
import { FormInputSpacing, FormOptionsWrapper } from "components/UI/Wrapper";
import SocialNetworkChip from "components/UI/Chip/SocialNetwork";
import CardHeaderTitleSubtitle from "components/UI/Card/Header/TitleSubtitle";
import CardContent from "components/UI/Card/Content";
// IMPORT from same folder
import { LoginFormWrapper, ButtonWrapper, SocialNetWrapper } from "./Wrapper";

// Custom Styles
const StyledCard = styled(Card)`
  width: 100%;
  max-width: 450px;

  @media screen and (min-width: 450px) {
    margin-top: ${props => props.theme.spacing.xl}px;
  }
`;

// Component: Stateful
class LoginOrSignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      login: this.props.login,
      showPassword: false
    };
  }

  render() {
    const {
      isLoading,
      title,
      subtitle,
      userLabel,
      userPlaceholder,
      forgotPassword,
      facebookText,
      googleText,
      footerText,
      termsOfUse,
      linkTo,
      history,
      intl
    } = this.props;

    return (
      <LoginFormWrapper>
        <StyledCard>
          {isLoading && <LinearProgress color="secondary" />}
          <CardContent>
            {/* FORM HEADER */}
            <CardHeaderTitleSubtitle title={title} subtitle={subtitle} />

            {/* FORM INPUTS */}
            <FormInputSpacing>
              <TextField
                disabled={isLoading}
                id="user"
                type="email"
                fullWidth
                onChange={this.handleChange("email")}
                label={intl.formatMessage({ id: userLabel })}
                placeholder={intl.formatMessage({ id: userPlaceholder })}
              />
            </FormInputSpacing>

            <FormInputSpacing>
              <PasswordWithToggleView
                disabled={isLoading}
                label="password-label"
                placeholder={intl.formatMessage({ id: "password-placeholder" })}
                password={this.state.password}
                handleChange={this.handleChange}
              />
            </FormInputSpacing>

            {/* FORM BUTTONS AND OPTIONS */}
            <ButtonWrapper>
              <RaisedAccentButton
                disabled={isLoading}
                onClick={() => this.loginOrSignUp(this.state, this.props)}
              >
                <FormattedMessage id={title} />
              </RaisedAccentButton>
              {forgotPassword && (
                <SmallAccentButton
                  onClick={() => history.push("/password/recover")}
                >
                  <FormattedMessage id={forgotPassword} />
                </SmallAccentButton>
              )}
            </ButtonWrapper>

            {termsOfUse && (
              <FormOptionsWrapper>
                <Typography variant="caption">
                  <FormattedMessage id={termsOfUse} />
                </Typography>
              </FormOptionsWrapper>
            )}

            <Divider />

            {/* SOCIAL NETWORK LOGIN OR SIGN UP */}
            {false && (
              <SocialNetWrapper>
                <SocialNetworkChip
                  buttonText={facebookText}
                  color="facebook"
                  icon={FacebookIcon}
                  handleClick={this.handleClick}
                />
                <SocialNetworkChip
                  buttonText={googleText}
                  color="googleRed"
                  icon={GoogleIcon}
                  handleClick={this.handleClick}
                />
              </SocialNetWrapper>
            )}
          </CardContent>

          {/* FORM FOOTER / ADDITIONAL OPTIONS */}
          <FormOptionsWrapper>
            <Link to={linkTo}>
              <Typography variant="caption">
                <FormattedMessage id={footerText} />
              </Typography>
            </Link>
          </FormOptionsWrapper>
        </StyledCard>
      </LoginFormWrapper>
    );
  }

  loginOrSignUp = (state, props) => {
    const { email, password } = state;
    const { locale } = props;
    let body = {
      email,
      password,
      locale
    };
    if (state.login) {
      props.logIn(body);
    } else {
      props.signUp(body);
    }
  };

  handleClick = () => {
    // Handle Facebook & Google Log In or Sign Up
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
}

// PropTypes
LoginOrSignUpForm.propTypes = {
  login: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  facebookText: PropTypes.string.isRequired,
  googleText: PropTypes.string.isRequired,
  footerText: PropTypes.string.isRequired,
  termsOfUse: PropTypes.string,
  forgotPassword: PropTypes.string,
  linkTo: PropTypes.string.isRequired
};
// Redux Store
const mapStateToProps = state => ({
  locale: state.app.locale,
  isLoading: state.app.is_loading
});
// Redux Dispatch
const mapDispatchToProps = dispatch => ({
  logIn: body => dispatch(logIn(body)),
  signUp: body => dispatch(signUp(body)),
  getProfile: body => dispatch(getProfile(body))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
  injectIntl
)(LoginOrSignUpForm);
