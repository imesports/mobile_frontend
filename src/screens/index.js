/**

  After creating a new Container import it here and export it further

  key word: NewContainer

*/
import Root from "./Root";

export default Root;
