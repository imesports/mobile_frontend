// IMPORT libs
import styled from "styled-components";
import Typography from "material-ui/Typography";

// Custom Styles
export const SignUpSubtitle = styled(Typography)`
  && {
    font-family: "Roboto Condensed", sans-serif;
    display: block;
    font-size: 1.2em;
    color: ${props => props.theme.text.secondary};
  }
`;

export const SignUpTitle = styled(Typography)`
  && {
    color: ${props => props.theme.text.white};
    display: block;
    font-size: 1.5em;
    font-weight: bold;
    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const FooterSubTitle = styled(Typography)`
  && {
    color: ${props => props.theme.text.footer};
    display: block;
    font-size: 1.05em;
    margin-bottom: ${props => props.theme.spacing.default}px;
  }
`;
