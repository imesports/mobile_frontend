// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
// Custom Styles
const IconWrapperMDI = styled.a`
  fill: ${props =>
    props.secondaryText ? props.theme.text.secondary : props.theme.text.footer};

  svg {
    width: ${props => props.theme.iconSize[props.iconSize]}px;
    height: ${props => props.theme.iconSize[props.iconSize]}px;
  }
`;
// Component: Stateless
const IconButtonWithOutsideLink = props => {
  const { link, iconSize, secondaryText } = props;
  return (
    <IconWrapperMDI
      secondaryText={secondaryText}
      iconSize={iconSize}
      key={link}
      href={link}
      target="_blank"
      rel="noopener noreferrer"
    >
      <props.icon />
    </IconWrapperMDI>
  );
};
// PropTypes
IconButtonWithOutsideLink.propTypes = {
  link: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  iconSize: PropTypes.oneOf(["small", "medium", "large", "xl"]).isRequired,
  secondaryText: PropTypes.bool
};

export default IconButtonWithOutsideLink;
