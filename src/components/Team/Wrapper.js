// IMPORT styled
import styled from "styled-components";
// IMPORT material
import Button from "material-ui/Button";

// Create/Form
export const CreateFormChooseAvatarPosition = styled.div`
   {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const CreateFormAvatarPosition = styled.div`
   {
    padding-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const CreateFormStyledButton = styled(Button)`
  && {
    font-size: 1em;
  }
`;

export const CreateFormDontHaveGameAlert = styled.div`
  && {
    display: flex;
    align-items: center;
    margin-top: ${props => props.theme.spacing.small}px;
  }
`;

export const CreateFormIconAlert = styled.div`
   {
    margin-right: ${props => props.theme.spacing.xxs}px;
  }
`;

//MyTeamsCard
export const MyTeamsCardFirstRow = styled.div`
   {
    display: flex;
    align-items: center;
    width: 100%;
  }
`;

export const MyTeamsCardSecondRow = styled.div`
   {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
  }
`;

export const MyTeamsCardNameAndMembers = styled.div`
   {
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 0 ${props => props.theme.spacing.small}px;
    width: calc(
      100% - ${props => props.theme.iconSize.xl}px -
        ${props => props.theme.iconSize.default}px
    );
  }
`;

export const MyTeamsCardRole = styled.div`
   {
    display: flex;
    align-items: center;
  }
`;

export const MyTeamsCardRoleName = styled.div`
   {
    padding-left: ${props => props.theme.spacing.xxs}px;
  }
`;

export const MyTeamsCardMemberSince = styled.div`
   {
    display: flex;
  }
`;
