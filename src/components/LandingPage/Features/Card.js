// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Card, { CardMedia } from "material-ui/Card";
import Typography from "material-ui/Typography";
// IMPORT components
import CardContent from "components/UI/Card/Content";
// Custom Styles
const StyledCard = styled(Card)`
  margin-bottom: ${props => props.theme.spacing.small}px;
  flex-grow: 1;

  @media screen and (min-width: 800px) {
    width: 250px;
    margin: 0 ${props => props.theme.spacing.xs}px
      ${props => props.theme.spacing.small}px;
  }
`;

const StyledCardMedia = styled(CardMedia)`
  height: 250px;
`;

// Component Stateless
const LandingPageFeaturesCard = ({ cardImage, title, subtitle }) => (
  <StyledCard>
    <StyledCardMedia
      image={cardImage}
      title={<FormattedMessage id={title} />}
    />
    <CardContent>
      <Typography variant="title">
        <FormattedMessage id={title} />
      </Typography>
      <Typography>
        <FormattedMessage id={subtitle} />
      </Typography>
    </CardContent>
  </StyledCard>
);

// PropTypes
LandingPageFeaturesCard.propTypes = {
  cardImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired
};

export default LandingPageFeaturesCard;
